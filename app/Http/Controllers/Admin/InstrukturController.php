<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Gate;
use Illuminate\Support\Facades\Auth;

class InstrukturController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
      if (Auth::check() && Gate::denies('admin-access')) {
        abort(403);
      }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = User::orderBy('id', 'DESC')->where('role', 'instruktur')->where('status', '1')->paginate(8);
      return view('admin.instruktur')->with('datas', $datas);
    }

    public function instrukturNonaktif()
    {
      $datas = User::orderBy('id', 'DESC')->where('role', 'instruktur')->where('status', '0')->paginate(8);
      return view('admin.instruktur-nonaktif')->with('datas', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.auth.addUser')->with('role', 'instruktur');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $status = 1;
      $this->validate($request, [
        'role' => 'required',
        'name' => 'required|max:50',
        'username' => 'required',
        'password' => 'required|confirmed',
        'email' => 'required|unique:users,email|email',
      ]);

      User::create([
        'name' => $request['name'],
        'username' => $request['username'],
        'email' => $request['email'],
        'password' => bcrypt($request['password']),
        'role' => $request['role'],
        'status' => $status,
      ]);

      session()->flash('flash_message', 'User berhasil ditambahkan!');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.auth.editUser')->with('data',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::findOrFail($id);
      if($user->email != $request->email) {
        $this->validate($request, [
          'name' => 'required|max:50',
          'email' => 'required|unique:users,email,$id,id|email',
        ]);
      } else {
        $this->validate($request, [
          'name' => 'required|max:50',
        ]);
      }

      $input = $request->all();

      $user->fill($input)->save();

      session()->flash('flash_message', 'Change(s) successfully saved!');

      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = User::findOrFail($id);

      $user->delete();

      session()->flash('flash_message', 'User successfully deleted!');

      return redirect()->route('admin.instruktur.index');
    }

    public function activate($id)
    {
      User::where('id', $id)->update(['status' => 1]);

      session()->flash('flash_message', 'User telah diaktifkan!');

      return redirect()->route('admin.instruktur.nonaktif');
    }

   
}
