<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\Kelas;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Gate;
use Illuminate\Support\Facades\Auth;

class KelasController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
      if (Auth::check() && Gate::denies('admin-access')) {
        abort(403);
      }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Kelas::orderBy('id', 'DESC')->paginate(8);
      return view('admin.kelas')->with('datas', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.auth.addKelas');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'nama_kelas' => 'required|max:50',
        'tahun_ajar_mulai' => 'required',
        'batch' => 'required',
      ]);

      Kelas::create([
        'nama_kelas' => $request['nama_kelas'],
        'tahun_ajar_mulai' => $request['tahun_ajar_mulai'],
        'batch' => $request['batch'],
      ]);

      session()->flash('flash_message', 'Kelas berhasil ditambahkan!');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::findOrFail($id);

        return view('admin.auth.editKelas')->with('data',$kelas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $kelas = Kelas::findOrFail($id);
      $this->validate($request, [
        'nama_kelas' => 'required|max:50',
        'tahun_ajar_mulai' => 'required|digits:4',
        'batch' => 'required',
      ]);

      $input = $request->all();

      $kelas->fill($input)->save();

      session()->flash('flash_message', 'Change(s) successfully saved!');

      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kelas = Kelas::findOrFail($id);

      $kelas->delete();

      session()->flash('flash_message', 'Kelas successfully deleted!');

      return redirect()->route('admin.kelas.index');
    }
}
