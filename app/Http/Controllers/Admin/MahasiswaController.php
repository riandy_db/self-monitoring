<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use DB;
use Excel;

class MahasiswaController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
      if (Auth::check() && Gate::denies('admin-access')) {
        abort(403);
      }
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$datas = User::orderBy('id', 'DESC')->where('role', 'mahasiswa')->paginate(8);
		return view('admin.mahasiswa')->with('datas', $datas);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.auth.addUser')->with('role', 'mahasiswa');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
      $this->validate($request, [
        'role' => 'required',
        'name' => 'required|max:50',
        'username' => 'required',
        'password' => 'required|confirmed',
        'email' => 'required|unique:users,email|email',
        'no_identitas' => 'required|digits:10',
      ]);

      User::create([
        'name' => $request['name'],
        'username' => $request['username'],
        'email' => $request['email'],
        'password' => bcrypt($request['password']),
        'role' => $request['role'],
        'no_identitas' => $request['no_identitas'],
      ]);

      session()->flash('flash_message', 'User berhasil ditambahkan!');

      return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
        $user = User::findOrFail($id);

        return view('admin.auth.editUser')->with('data',$user);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
      $user = User::findOrFail($id);
      if($user->email != $request->email) {
        $this->validate($request, [
          'name' => 'required|max:50',
          'email' => 'required|unique:users,email,$id,id|email',
          'no_identitas' => 'required|digits:10',
        ]);
      } else {
        $this->validate($request, [
          'name' => 'required|max:50',
          'no_identitas' => 'required|digits:10',
        ]);
      }

      $input = $request->all();

      $user->fill($input)->save();

      session()->flash('flash_message', 'Change(s) successfully saved!');

      return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$user = User::findOrFail($id);

    $user->delete();

    session()->flash('flash_message', 'User successfully deleted!');

    return redirect()->route('admin.mahasiswa.index');
	}

	public function import()
	{
			return view('admin.auth.importUsers');
	}

	public function storeImport()
	{
		if(Input::hasFile('daftar_mahasiswa')){
			$path = Input::file('daftar_mahasiswa')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = ['no_identitas' => $value->npm, 'username' => $value->username, 'name' => $value->nama, 'password' => bcrypt($value->password), 'email' => $value->email, 'role' => 'mahasiswa' ];
				}
				if(!empty($insert)){
					DB::table('users')->insert($insert);
				}
			}
		}
		session()->flash('flash_message', 'Data successfully imported!');
		return back();
	}

}
