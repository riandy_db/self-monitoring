<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Image;
use File;

use Illuminate\Support\Facades\Auth;

class AvatarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('auth.change-avatar');
    }

    public function set(Request $request){
    // Handle the user upload of avatar
			$avatar = $request->file('avatar');

			$messages = [	'avatar.required' => 'The :attribute photo field is required.',
										'avatar.size' => 'The file size must not exceed :size.',
										'avatar.mimes' => 'The :attribute must be one of the following types: :values'
									];
		// Create avatar upload rules				
	    $v = Validator::make($request->all(), [
	        'avatar' => 'required|mimes:jpg,jpeg,png|max:2000',
	    ], $messages);
	  // Handle avatar upload error
	    if ($v->fails())
	    {
	        return redirect()->back()->withErrors($v->errors());
	    }

      $path = base_path();
      $path = $path . '/public/';

	  // Store avatar image
			$filename = time() . '.' . $avatar->getClientOriginalExtension();
			Image::make($avatar)->resize(300, 300)->save( $path . 'img/avatar/' . $filename );


			$user = Auth::user();
		// Remove unecessary avatar image
			if($user->url_foto != 'default.png')
			{
				File::Delete( $path . 'img/avatar/' . $user->url_foto );

			}
		// Set new avatar
			$user->url_foto = $filename;
			$user->save();

			return redirect('/');
	}
}
