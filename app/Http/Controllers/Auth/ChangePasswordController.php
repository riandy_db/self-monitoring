<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('auth.passwords.change');
    }

    protected function set(Request $request)
    {
      $this->validate($request, [
        'oldPassword' => 'required',
        'password' => 'required|min:6|confirmed',
      ]);

      if (Hash::check($request['oldPassword'], Auth::user()->password)) {
      	User::where('username', Auth::user()->username)
      			->update(['password' => Hash::make($request['password'])]);
        session()->flash('flash_message', 'Password berhasil diubah!');
			} else {
				session()->flash('flash_error', 'Password lama tidak sesuai!');
				return redirect()->back();
			}
      return redirect('/mahasiswa');
    }
}
