<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomAuthController extends Controller
{
  
  public function showLoginForm() 
  { 
    return view('auth.login-new');
  }
  
  public function login(Request $request) 
  { 
    $active = '1';
    
    $this->validate($request, [
      'username' => 'required|exists:users',
      'password' => 'required',
    ]);

    if (Auth::attempt([
      'username'=>$request->username, 
      'password'=>$request->password,
      'status'=>$active
    ])) {
      
      $user = User::where('username', $request->username)->first();
      if ($user->role == 'admin') {
        return redirect()->route('admin.index');
      } 
      else if ($user->role == 'instruktur') {
        return redirect('/instruktur/prompt');
      }
      return redirect('/mahasiswa');
      
    }
    session()->flash('flash_message', 'Username or password is incorrect.');
    return redirect('/login');
    
  }


  public function showRegisterUser() 
  {
  	return view('auth.register-user');
  }

  public function showRegisterMahasiswa() 
  {
    return view('auth.register-mahasiswa');
  }

  public function registerMahasiswa(Request $request) 
  {
    $this->validationMahasiswa($request);
    return $this->storeMahasiswa($request);  
  }

  public function validationMahasiswa($request)
  {
    return $this->validate($request, [
      'name' => 'required|max:255',
      'username' => 'required|max:50|unique:users',
      'password' => 'required|min:6|confirmed',
      'email' => 'required|unique:users,email|email',
      'no_identitas' => 'required|digits_between:10,15',
      ]);    
  }

  public function storeMahasiswa($request)
  {
    $role = 'mahasiswa';
    $status = '1';
    User::create([
      'name' => $request['name'],
      'username' => $request['username'],
      'email' => $request['email'],
      'password' => bcrypt($request['password']),
      'role' => $role,
      'status' => $status,
      'no_identitas' => $request['no_identitas'],
      ]);
    return redirect('/login')->with('status', 'mahasiswa');
  } 
  
  public function showRegisterInstruktur() 
  {
    return view('auth.register-instruktur');
  }

  public function registerInstruktur(Request $request) 
  {
    $this->validationInstruktur($request);
    return $this->storeInstruktur($request);  
  }

  public function validationInstruktur(Request $request)
  {
    return $this->validate($request, [
      'name' => 'required|max:255',
      'username' => 'required|max:50|unique:users',
      'password' => 'required|min:6|confirmed',
      'email' => 'required|unique:users,email|email',
      ]);
  }

  public function storeInstruktur($request)
  {
   $role = 'instruktur';
   $status = '0';
   User::create([
    'name' => $request['name'],
    'username' => $request['username'],
    'email' => $request['email'],
    'password' => bcrypt($request['password']),
    'role' => $role,
    'status' => $status,
    ]);
   return redirect('/login')->with('status', 'instruktur');
 }



}
