<?php

namespace App\Http\Controllers;

use Gate;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (Gate::allows('admin-access')) {
        return redirect()->route('admin.index');
      }
      else if (Gate::allows('instruktur-access')) {
        return redirect('/instruktur/kelas');
      }
      else { 
        return redirect('mahasiswa');
      }
    }
}
