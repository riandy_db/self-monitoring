<?php

namespace App\Http\Controllers\Instruktur;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Jawaban;
use App\Models\Feedback;
use App\Models\FeedbackJawaban;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
	  public function storeFeedback(Request $request)
    {
      $prompt = FeedbackJawaban::firstOrCreate([
        'id_jawaban' => $request['id_jawaban'],
        'id_feedback' => $request['feedback'],
      ]);

      session()->flash('flash_message', 'Feedback berhasil disimpan!');

      return redirect()->back();
    }

    public function storeAnswer(Request $request)
    {
      $jawaban = '';

      if ($request['feedback'] == null) {
          $jawaban = $request['jawaban'];
          $this->saveAnswer($request, $jawaban);    
      } else {
        $feedback = Feedback::where('id', $request['feedback'])->get()->first();
        $jawaban = $feedback->feedback_default;
        $this->saveAnswer($request, $jawaban);
      }

      session()->flash('flash_message', 'Balasan berhasil disimpan!');

      return redirect()->back(); 
    }

    public function saveAnswer(Request $request, $jawaban)
    {
        $prompt = new Jawaban;
        $prompt->id_user = Auth::user()->id;
        $prompt->id_user_sentTo = $request->id_user_sentTo;
        $prompt->id_prompt = $request->id_prompt;
        $prompt->jawaban = $jawaban;
        
        $prompt->save();

    }
}
