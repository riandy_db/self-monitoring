<?php

namespace App\Http\Controllers\Instruktur;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Kelas;
use App\Models\KelasUser;

use App\Http\Requests;
use App\Http\Controllers\Controller;

Use Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use DB;
use Excel;

class KelasController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
      if (Auth::check() && Gate::denies('instruktur-access')) {
        abort(403);
      }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $kelasUser = User::findOrFail(Auth::user()->id)->classes()->orderBy('nama_kelas','asc')->get();
      // $kelasList = $kelasUser->map(function ($item, $key){
      //   return $item->pivot->id_kelas;
      // });
      // $userKelas = Kelas::find(13)->users()->get();
      return view('instruktur.kelas', ['datas'=> $kelasUser]);
    }

    /**
     * Show the form for assigning class.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignClass()
    {
      $kelas = Kelas::all();
      $kelasUser = User::findOrFail(Auth::user()->id)->classes()->get();
      $kelasDiff = $kelas->diff($kelasUser);
      $kelasDiffMap = $kelasDiff->each(function ($item, $key) {
        $item->nama_kelas = $item->nama_kelas . ' (' . $item->tahun_ajar_mulai . ' ' . ucfirst($item->batch) . ')';
      })->sortBy('nama_kelas')->pluck('nama_kelas','id');
     // $kelasDiff = $kelasDiff->pluck('nama_kelas','id');

      $mahasiswa = User::where('role','mahasiswa')->get();
      $mahasiswaMap = $mahasiswa ->each(function ($item, $key) {
        $item->name = $item->name . ' (' . $item->no_identitas . ')';
      })->sortBy('no_identitas')->pluck('name','id');

          // var data = [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }];
      return view('instruktur.assignKelas', ['kelas' => $kelasDiffMap, 'mahasiswa' => $mahasiswaMap]);
    }

    public function addMahasiswa($id)
    {
      $kelasUser = KelasUser::where('id_kelas', $id)->get();
      $isValid = $kelasUser->contains('id_user', Auth::user()->id);
      if (!$isValid) {
        abort(403);
      }

      $kelasInfo = Kelas::findOrFail($id);
      $userKelas = $kelasInfo->users()->where('role','mahasiswa')->orderBy('name','ASC')->get();
      $userAvailable = User::where('role','mahasiswa')->get()->diff($userKelas)->each(function ($item, $key) {
        $item->name = $item->name . ' (' . $item->no_identitas . ')';
      })->sortBy('name')->pluck('name','id');

      return view('instruktur.addMahasiswa', ['kelasInfo' => $kelasInfo, 'userAvailable' => $userAvailable]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      KelasUser::create([
        'id_user' => Auth::user()->id,
        'id_kelas' => $request['nama_kelas']
      ]);

      if($request['daftar_mahasiswa']){
        foreach ($request['daftar_mahasiswa'] as $key => $value) {
          $insert[] = [
            'id_user' => $value,
            'id_kelas' => $request['nama_kelas'] 
            ];
        }
        if(!empty($insert)){
          DB::table('kelas_users')->insert($insert);
        }
      }

      session()->flash('flash_message', 'Kelas berhasil ditambahkan!');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $kelasUser = KelasUser::where('id_kelas', $id)->get();
      $isValid = $kelasUser->contains('id_user', Auth::user()->id);
      if (!$isValid) {
        abort(403);
      }
      
      $kelasInfo = Kelas::findOrFail($id);
      $userKelas = $kelasInfo->users()->where('role','mahasiswa')->orderBy('no_identitas','ASC')->paginate(8);
      return view('instruktur.singleKelas', ['kelasInfo' => $kelasInfo, 'userKelas' => $userKelas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function import($id)
    {
      $kelasUser = KelasUser::where('id_kelas', $id)->get();
      $isValid = $kelasUser->contains('id_user', Auth::user()->id);
      if (!$isValid) {
        abort(403);
      }

      $kelas = Kelas::findOrFail($id);

      return view('instruktur.importAssignMahasiswa', ['kelas' => $kelas]);
    }

    public function storeImport(Request $request)
    {
      $userAvailable = User::where('role','mahasiswa')->get();
      if(Input::hasFile('daftar_mahasiswa')){
        $path = Input::file('daftar_mahasiswa')->getRealPath();
        $data = Excel::load($path, function($reader) {
        })->get();
        if(!empty($data) && $data->count()){
          foreach ($data as $key => $value) {
            $username = $value->username;
            $userList = $userAvailable;
            $userId = $userList->flatten()->where('username', $username)->last()->id;
            $insert[] = ['id_user' => $userId, 'id_kelas' => $request['id_kelas'] ]; 
          }
         // dd($insert);
          if(!empty($insert)){
            DB::table('kelas_users')->insert($insert);
          }
        }
      }
      session()->flash('flash_message', 'Data successfully imported!');
      return back();
      /*
        - ambil seluruh data mahasiswa -> toArray
        - tentukan kelasnya -> getId
        - arrayOfMahasiswa -> whereIn (excel mahasiswa.npm)
        - $insert[](idKelas, idUser)
        - masukkin $insert [array of mahasiswa+kelas] ke db
      */
    }
}
