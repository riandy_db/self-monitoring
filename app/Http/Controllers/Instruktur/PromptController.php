<?php

namespace App\Http\Controllers\Instruktur;

use Illuminate\Http\Request;

use App\Models\KelasUser;
use App\Models\Kelas;
use App\Models\Prompt;
use App\Models\User;
use App\Models\KataKunciPrompt;
use App\Models\Feedback;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Gate;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class PromptController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
      if (Auth::check() && Gate::denies('instruktur-access')) {
        abort(403);
      }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $kelasUser = User::findOrFail(Auth::user()->id)->classes()->orderBy('nama_kelas','asc')->get();
      $promptKelas = count($kelasUser) > 0 ? Kelas::findOrFail($kelasUser[0]->id)->prompt()->orderBy('created_at','desc')->get() : null;
      return view('instruktur.prompt', ['daftarKelas' => $kelasUser, 'daftarPrompt' => $promptKelas, 'idKelas' => 0]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $kelasUser = User::findOrFail(Auth::user()->id)->classes()->orderBy('nama_kelas','asc')->get();
      $kelasUser = $kelasUser->each(function ($item, $key) {
        $item->nama_kelas = $item->nama_kelas . ' (' . $item->tahun_ajar_mulai . ' ' . ucfirst($item->batch) . ')';
      })->sortBy('nama_kelas')->pluck('nama_kelas','id');
      return view('instruktur.addPrompt', ['daftarKelas' => $kelasUser]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'id_kelas' => 'required',
        'topik' => 'required|max:50',
        'pertanyaan' => 'required',
      ]);

      $prompt = Prompt::create([
        'id_kelas' => $request['id_kelas'],
        'topik' => $request['topik'],
        'pertanyaan' => $request['pertanyaan'],
        'created_at' => Carbon::now(),
      ]);

      $kataKunci = explode(';', $request['kata_kunci']);
      $feedbackDefault = explode(';', $request['feedback_default']);
      $idPrompt = $prompt->id;

      foreach ($kataKunci as $key => $value) {
        KataKunciPrompt::firstOrCreate([
          'id_prompt' => $idPrompt,
          'katakunci' => trim($value),
        ]);
      }

      foreach ($feedbackDefault as $key => $value) {
        Feedback::firstOrCreate([
          'id_prompt' => $idPrompt,
          'feedback_default' => trim($value),
        ]);
      }

      session()->flash('flash_message', 'Prompt berhasil ditambahkan!');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('instruktur.prompt', ['daftarKelas' => $kelasUser, 'daftarPrompt' => $promptKelas, 'index' => false]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showPrompts($id_kelas)
    {
      $kelasUser = KelasUser::where('id_kelas', $id_kelas)->get();
      $isValid = $kelasUser->contains('id_user', Auth::user()->id);
      if (!$isValid) {
        abort(403);
      }
      $kelasUser = User::findOrFail(Auth::user()->id)->classes()->orderBy('nama_kelas','asc')->get();
      $promptKelas = Kelas::findOrFail($id_kelas)->prompt()->orderBy('created_at','desc')->get();
      return view('instruktur.prompt', ['daftarKelas' => $kelasUser, 'daftarPrompt' => $promptKelas, 'idKelas' => $id_kelas]);
    }
}
