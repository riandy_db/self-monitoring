<?php

namespace App\Http\Controllers\Instruktur;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\KelasUser;

class SingleKelasController extends Controller
{
  public function destroyMahasiswa($id_user, $id_kelas)
  {
    $kelasUser = KelasUser::where(['id_user' => $id_user, 'id_kelas' => $id_kelas]);
    $kelasUser->delete();

    session()->flash('flash_message', 'User successfully deleted!');

    return redirect()->back();
  }
  public function destroyKelas($id_user, $id_kelas)
  {
    $kelasUser = KelasUser::where(['id_user' => $id_user, 'id_kelas' => $id_kelas]);
    $kelasUser->delete();

  	session()->flash('flash_message', 'Kelas successfully deleted!');

    return redirect()->back();
  }
}
