<?php

namespace App\Http\Controllers\Instruktur;

use Illuminate\Http\Request;

use App\Models\KelasUser;
use App\Models\Kelas;
use App\Models\Prompt;
use App\Models\User;
use App\Models\Jawaban;
use App\Models\Feedback;
use App\Models\FeedbackJawaban;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Gate;
use Illuminate\Support\Facades\Auth;

class SinglePromptController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
      if (Auth::check() && Gate::denies('instruktur-access')) {
        abort(403);
      }
    }
    
    public function showSinglePrompt($id_kelas, $id_prompt)
    {
      $kelasUser = KelasUser::where('id_kelas', $id_kelas)->get();
      $isValidKelas = $kelasUser->contains('id_user', Auth::user()->id);
      $userPrompt = Prompt::where('id', $id_prompt)->get()->pop();
      $isValidPrompt = $userPrompt != null ? $userPrompt->id_kelas == $id_kelas : false;
      if (!$isValidKelas || !$isValidPrompt) {
        abort(403);
      }

      $kelas = Kelas::where('id', $id_kelas)->get()->pop();
      $prompt = Prompt::where('id', $id_prompt)->get()->pop();
      $kataKunci = Prompt::findOrFail($id_prompt)->kataKunci()->orderBy('katakunci','asc')->get();
      $jawabanPrompt = Jawaban::where('id_prompt', $id_prompt)->get();
      $feedbackPrompt = Feedback::where('id_prompt', $id_prompt)->get()->pluck('feedback_default','id');

      $jawabanUser = collect();

      foreach ($jawabanPrompt as $key => $value) {
        $user = Jawaban::findOrFail($value->id)->user()->get()->pop();
        $jawabanUser->push($user);
      }

      $feedbackInstruktur = collect();
      $feedbackJawaban = collect();

      foreach ($jawabanPrompt as $key => $value) {
        $feedbackId = FeedbackJawaban::where('id_jawaban',$value->id)->get()->pop();
        if($feedbackId!=null)
        { 
          $feedbackJawaban->push($feedbackId->balasan);
          $feedbackInstruktur->push(Feedback::findOrFail($feedbackId->id_feedback));
        }
        else
        {
          $feedbackInstruktur->push(null);
        }
      }
      return view('instruktur.singlePrompt', ['kelasInfo' => $kelas, 'prompt' => $prompt, 'kataKunci' => $kataKunci, 'jawabanPrompt' => $jawabanPrompt, 'jawabanUser' => $jawabanUser, 'feedbackPrompt' => $feedbackPrompt, 'feedbackJawaban' => $feedbackJawaban , 'feedbackInstruktur' => $feedbackInstruktur , 'isFirst' => false]);
    }

    public function destroySinglePrompt(Request $request)
    {
    	$prompt = Prompt::where('id', $request['id_prompt'])->delete();

	  	session()->flash('flash_message', 'Prompt successfully deleted!');

	    return redirect(route('instruktur.prompt.index'));
    }


    public function showSinglePromptNew($id_kelas, $id_prompt)
    {
      $kelasUser = KelasUser::where('id_kelas', $id_kelas)->get();
      $isValidKelas = $kelasUser->contains('id_user', Auth::user()->id);
      $userPrompt = Prompt::where('id', $id_prompt)->get()->pop();
      $isValidPrompt = $userPrompt != null ? $userPrompt->id_kelas == $id_kelas : false;
      if (!$isValidKelas || !$isValidPrompt ) {
        abort(403);
      }
      $kelas = Kelas::where('id', $id_kelas)->get()->pop();
      $prompt = Prompt::where('id', $id_prompt)->get()->pop();
      $kataKunci = Prompt::findOrFail($id_prompt)->kataKunci()->orderBy('katakunci','asc')->get();
      
      $jawabanUser = Jawaban::where('id_prompt', $id_prompt)->join('users', 'users.id', '=', 'jawaban.id_user')->where('users.role', 'mahasiswa')->groupBy('jawaban.id_user')->get();

    

      return view('instruktur.singlePromptNew', ['kelasInfo' => $kelas, 'prompt' => $prompt, 'kataKunci' => $kataKunci, 'jawabanUser' => $jawabanUser ]);
     
    }

    public function showSinglePromptUser($id_kelas, $id_prompt, $id_user)
    {
      $kelasUser = KelasUser::where('id_kelas', $id_kelas)->get();
      $isValidKelas = $kelasUser->contains('id_user', Auth::user()->id);
      $userPrompt = Prompt::where('id', $id_prompt)->get()->pop();
      $isValidPrompt = $userPrompt != null ? $userPrompt->id_kelas == $id_kelas : false;
      if (!$isValidKelas || !$isValidPrompt ) {
        abort(403);
      }
      $kelas = Kelas::where('id', $id_kelas)->get()->pop();
      $prompt = Prompt::where('id', $id_prompt)->get()->pop();
      $kataKunci = Prompt::findOrFail($id_prompt)->kataKunci()->orderBy('katakunci','asc')->get();


      $balasanMahasiswa = ['id_prompt' => $id_prompt, 'id_user' => $id_user];
      $balasanInstruktur = ['id_prompt' => $id_prompt, 'id_user_sentTo' => $id_user];
      $jawabanPrompt = Jawaban::where($balasanMahasiswa)->orWhere($balasanInstruktur)->get();

      $feedbackPrompt = Feedback::where('id_prompt', $id_prompt)->get()->pluck('feedback_default','id');
      
      // user yang memiliki jawaban
      $jawabanUser = collect();

      foreach ($jawabanPrompt as $key => $value) {
        $user = Jawaban::findOrFail($value->id)->user()->get()->pop();
        $jawabanUser->push($user);
      }

      return view('instruktur.singlePromptUser', ['id_user' => $id_user, 'kelasInfo' => $kelas, 'prompt' => $prompt, 'kataKunci' => $kataKunci, 'jawabanUser' => $jawabanUser, 'feedbackPrompt' => $feedbackPrompt, 'jawabanPrompt' => $jawabanPrompt ]);
     
    }
}
