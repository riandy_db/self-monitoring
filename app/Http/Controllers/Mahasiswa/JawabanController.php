<?php

namespace App\Http\Controllers\Mahasiswa;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Jawaban;
use App\Models\FeedbackJawaban;

use Illuminate\Support\Facades\Auth;

class JawabanController extends Controller
{
    public function storeJawaban(Request $request)
    {
      $this->validate($request, [
        'jawaban' => 'required',
      ]);

      $prompt = Jawaban::firstOrCreate([
        'id_user' => Auth::user()->id,
        'id_prompt' => $request['id_prompt'],
        'jawaban' => $request['jawaban'],
      ]);

      session()->flash('flash_message', 'Jawaban berhasil disimpan!');

      return redirect()->back();
    }

    public function storeBalasan(Request $request)
    {
      $this->validate($request, [
        'balasan' => 'required',
      ]);

      $balasan = FeedbackJawaban::where(['id_jawaban' => $request['id_jawaban'], 'id_feedback' => $request['id_feedback']])
          ->update(['balasan' => $request['balasan']]);

      session()->flash('flash_message', 'Jawaban berhasil disimpan!');

      return redirect()->back();
    }


    public function storeJawabanTemp(Request $request)
    {
      $this->validate($request, [
        'jawaban' => 'required',
      ]);

      $prompt = new Jawaban;
      $prompt->id_user = Auth::user()->id;
      $prompt->id_prompt = $request->id_prompt;
      $prompt->jawaban = $request['jawaban'];
        
      $prompt->save();

      session()->flash('flash_message', 'Jawaban berhasil disimpan!');

      return redirect()->back();
    }



}
