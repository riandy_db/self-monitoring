<?php

namespace App\Http\Controllers\Mahasiswa;

use Illuminate\Http\Request;

use App\Models\KelasUser;
use App\Models\Kelas;
use App\Models\Prompt;
use App\Models\User;
use App\Models\Jawaban;
use App\Models\FeedbackJawaban;
use App\Models\Feedback;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Gate;
use Illuminate\Support\Facades\Auth;

class SinglePromptController extends Controller
{
    public function showSinglePrompt($id_kelas, $id_prompt)
    {
      $kelasUser = KelasUser::where('id_kelas', $id_kelas)->get();
      $isValidKelas = $kelasUser->contains('id_user', Auth::user()->id);
      $userPrompt = Prompt::where('id', $id_prompt)->get()->pop();
      $isValidPrompt = $userPrompt != null ? $userPrompt->id_kelas == $id_kelas : false;
      if (!$isValidKelas || !$isValidPrompt ) {
        abort(403);
      }

      $kelas = Kelas::where('id', $id_kelas)->get()->pop();
      $prompt = Prompt::where('id', $id_prompt)->get()->pop();
      $kataKunci = Prompt::findOrFail($id_prompt)->kataKunci()->orderBy('katakunci','asc')->get();
      $jawabanPrompt = Jawaban::where(['id_prompt' => $id_prompt, 'id_user' => Auth::user()->id])->get()->pop();


      $feedbackInstruktur = '';
      $feedbackJawaban = '';
      if($jawabanPrompt!=null){
        $feedbackId = FeedbackJawaban::where('id_jawaban',$jawabanPrompt->id)->get()->pop();
        if($feedbackId!=null)
          {
            $feedbackInstruktur = Feedback::findOrFail($feedbackId->id_feedback);
            $feedbackJawaban = $feedbackId->balasan;
          }
        else
          {$feedbackInstruktur = $feedbackJawaban = null;}
      } else {
        $feedbackInstruktur = null;
      }

      return view('mahasiswa.prompt', ['kelasInfo' => $kelas, 'prompt' => $prompt, 'kataKunci' => $kataKunci, 'jawaban' => $jawabanPrompt, 'feedbackInstruktur' => $feedbackInstruktur, 'feedbackJawaban' => $feedbackJawaban, 'isFirst' => false]);
    }

    public function destroySinglePrompt($id_prompt)
    {
    	$prompt = Prompt::where('id', $id_prompt)->delete();

	  	session()->flash('flash_message', 'Prompt successfully deleted!');

	    return redirect(route('instruktur.prompt.index'));
    }

    public function showSinglePromptNew($id_kelas, $id_prompt)
    {
      $kelasUser = KelasUser::where('id_kelas', $id_kelas)->get();
      $isValidKelas = $kelasUser->contains('id_user', Auth::user()->id);
      $userPrompt = Prompt::where('id', $id_prompt)->get()->pop();
      $isValidPrompt = $userPrompt != null ? $userPrompt->id_kelas == $id_kelas : false;
      if (!$isValidKelas || !$isValidPrompt ) {
        abort(403);
      }

      $kelas = Kelas::where('id', $id_kelas)->get()->pop();
      $prompt = Prompt::where('id', $id_prompt)->get()->pop();
      $kataKunci = Prompt::findOrFail($id_prompt)->kataKunci()->orderBy('katakunci','asc')->get();

      $balasanMahasiswa = ['id_prompt' => $id_prompt, 'id_user' => Auth::user()->id];
      $balasanInstruktur = ['id_prompt' => $id_prompt, 'id_user_sentTo' => Auth::user()->id];
      $jawabanPrompt = Jawaban::where($balasanMahasiswa)->orWhere($balasanInstruktur)->get();

      // user yang memiliki jawaban
      $jawabanUser = collect();

      foreach ($jawabanPrompt as $key => $value) {
        $user = Jawaban::findOrFail($value->id)->user()->get()->pop();
        $jawabanUser->push($user);
      }


      return view('mahasiswa.promptNew', ['kelasInfo' => $kelas, 'prompt' => $prompt, 'kataKunci' => $kataKunci, 'jawabanPrompt' => $jawabanPrompt, 'jawabanUser' => $jawabanUser, 'isFirst' => false]);
    }
}
