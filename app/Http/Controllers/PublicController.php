<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PublicController extends Controller
{
    public function showGuide()
    {
      return view('public.guide');
    }

    public function showPublication()
    {
      return view('public.publication');
    }

    public function showDescription()
    {
      return view('public.description');
    }

}
