<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('instruktur/kelas/assign', function(){
// 	return view('instruktur.assignKelas');
// });

// Route::get('instruktur/prompt', function(){
// 	return view('instruktur.prompt');
// });

// Route::get('instruktur/prompt/create', function(){
// 	return view('instruktur.addPrompt');
// });




Route::get('change-avatar', [
    'as' => 'change-avatar',
    'uses' => 'Auth\AvatarController@index',
]);
Route::post('change-avatar/submit', [ 
    'as' => 'set-avatar',
    'uses' =>'Auth\AvatarController@set',
]);
// // Testing route above

Route::auth();

Route::get('change-password', [ 
    'as' => 'change-password',
    'uses' =>'Auth\ChangePasswordController@index',
]);
Route::post('set-password', [ 
    'as' => 'set-password',
    'uses' =>'Auth\ChangePasswordController@set',
]);


Route::get('/', 'HomeController@index');

Route::get('guide', 'PublicController@showGuide');

Route::get('publication', 'PublicController@showPublication');

Route::get('description', 'PublicController@showDescription');


Route::post('mahasiswa/store-balasan', [
    'as' => 'store-balasan-mahasiswa', 
    'uses' => 'Mahasiswa\JawabanController@storeBalasan'
]);

Route::post('mahasiswa/store-answer', [
    'as' => 'store-answer-mahasiswa', 
    'uses' => 'Mahasiswa\JawabanController@storeJawaban'
]);

Route::get('mahasiswa/kelas/{id}', [ 
    'as' => 'show-prompts-mahasiswa',
    'uses' =>'Mahasiswa\PromptController@showPrompts',
])->where('id', '[0-9]+');

Route::resource('mahasiswa/','Mahasiswa\PromptController');


// Baru single prompt mahasiswa

Route::post('mahasiswa/store-answer', [
    'as' => 'store.answer.mahasiswa', 
    'uses' => 'Mahasiswa\JawabanController@storeJawabanTemp'
]);

Route::get('mahasiswa/kelas/{id_kelas}/prompt/{id_prompt}', [ 
    'as' => 'show.single.prompt.mahasiswa',
    'uses' =>'Mahasiswa\SinglePromptController@showSinglePromptNew',
])->where('id_kelas', '[0-9]+');


// Baru single prompt instruktur

Route::get('instruktur/prompt/kelas/{id_kelas}/prompt/{id_prompt}', [ 
    'as' => 'show.single.prompt',
    'uses' =>'Instruktur\SinglePromptController@showSinglePromptNew',
])->where('id_kelas', '[0-9]+');

Route::get('instruktur/prompt/kelas/{id_kelas}/prompt/{id_prompt}/user/{id_user}', [ 
    'as' => 'show.single.prompt.user',
    'uses' =>'Instruktur\SinglePromptController@showSinglePromptUser',
])->where('id_kelas', '[0-9]+');

Route::post('instruktur/store-answer', [
    'as' => 'store.answer.instruktur', 
    'uses' => 'Instruktur\FeedbackController@storeAnswer'
]);




Route::post('instruktur/delete-prompt', [
    'as' => 'destroy-prompt', 
    'uses' => 'Instruktur\SinglePromptController@destroySinglePrompt'
]);

Route::get('instruktur/prompt/kelas/{id}', [ 
	'as' => 'show-prompts',
	'uses' =>'Instruktur\PromptController@showPrompts',
])->where('id', '[0-9]+');
Route::resource('instruktur/prompt','Instruktur\PromptController');




Route::post('instruktur/delete-mahasiswa/{id_user}/{id_kelas}', [
    'as' => 'destroy-mahasiswa', 
    'uses' => 'Instruktur\SingleKelasController@destroyMahasiswa'
]);
Route::post('instruktur/delete-kelas/{id_user}/{id_kelas}', [
    'as' => 'destroy-kelas', 
    'uses' => 'Instruktur\SingleKelasController@destroyKelas',
]);
Route::resource('instruktur/mahasiswa', 'Instruktur\MahasiswaController', ['parameters' => ['id' => 'id' ]]);




Route::get('instruktur/kelas/{id}/import-mahasiswa', [ 
    'as' => 'instruktur.kelas.import',
    'uses' =>'Instruktur\KelasController@import',
])->where('id', '[0-9]+');
Route::post('instruktur/kelas/storeImport', [
    'as' => 'instruktur.kelas.storeImport', 
    'uses' => 'Instruktur\KelasController@storeImport',
]);
Route::get('instruktur/kelas/{id}/tambah-mahasiswa', 'Instruktur\KelasController@addMahasiswa')->where('id', '[0-9]+');
Route::get('instruktur/kelas/{id}', 'Instruktur\KelasController@show')->where('id', '[0-9]+');
Route::get('instruktur/kelas/assign', 'Instruktur\KelasController@assignClass');
Route::resource('instruktur/kelas','Instruktur\KelasController');


Route::get('admin/mahasiswa/import', [ 
    'as' => 'admin.mahasiswa.import',
    'uses' =>'Admin\MahasiswaController@import',
]);
Route::post('admin/mahasiswa/storeImport', [
    'as' => 'admin.mahasiswa.storeImport', 
    'uses' => 'Admin\MahasiswaController@storeImport',
]);

Route::get('admin/instruktur/nonaktif', [
    'as' => 'admin.instruktur.nonaktif', 
    'uses' => 'Admin\InstrukturController@instrukturNonaktif',
]);

Route::get('admin/instruktur/activate/{id}', [
    'as' => 'admin.instruktur.activate', 
    'uses' => 'Admin\InstrukturController@activate',
]);


Route::resource('admin/kelas','Admin\KelasController');
Route::resource('admin/instruktur','Admin\InstrukturController');
Route::resource('admin/mahasiswa','Admin\MahasiswaController');
Route::resource('admin','Admin\AdminController');




// Register user (baru)

Route::get('register/user', 'CustomAuthController@showRegisterUser');
Route::get('register/mahasiswa', 'CustomAuthController@showRegisterMahasiswa');
Route::post('register/mahasiswa', 'CustomAuthController@registerMahasiswa');
Route::get('register/instruktur', 'CustomAuthController@showRegisterInstruktur');
Route::post('register/instruktur', 'CustomAuthController@registerInstruktur');

// Login new

Route::post('login/user', [
    'as' => 'login.custom', 
    'uses' => 'CustomAuthController@login',
]);
