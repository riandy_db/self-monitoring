<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
	protected $table = 'feedback';

	public $timestamps = false;

	protected $fillable = [
  	'id_prompt', 'feedback_default', 'butuh_balasan'
  ];

	public function prompt(){
		return $this->belongsTo('App\Models\Prompt','id_prompt','id');
	}
}
