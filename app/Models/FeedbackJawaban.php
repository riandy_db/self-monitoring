<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackJawaban extends Model
{
	protected $table = 'feedback_jawaban';

	public $timestamps = false;

  protected $fillable = [
  	'id_feedback', 'id_jawaban'
  ];

	public function feedback(){
		return $this->belongsTo('App\Models\Feedback','id_feedback');
	}

}
