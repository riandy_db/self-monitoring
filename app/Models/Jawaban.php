<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
	protected $table = 'jawaban';

  protected $fillable = [
  	'id_prompt', 'id_user', 'id_user_sentTo', 'jawaban',
  ];

  public function prompt(){
  	return $this->belongsTo('App\Models\Prompt','id_prompt','id');
  }

  public function user(){
  	return $this->belongsTo('App\Models\User','id_user','id');
  }

}
