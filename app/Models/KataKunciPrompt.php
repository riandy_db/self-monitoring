<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KataKunciPrompt extends Model
{
	protected $table = 'katakunci_prompt';

	public $timestamps = false;

  protected $fillable = [
  	'id_prompt', 'katakunci',
  ];

	public function prompt(){
		return $this->belongsTo('App\Models\Prompt','id_prompt','id');
	}

}
