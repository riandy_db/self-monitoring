<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
  public $timestamps = false;

  protected $dateFormat = 'Y';

  protected $fillable = [
  	'nama_kelas', 'tahun_ajar_mulai', 'batch'
  ];

  public function users()
  {
    return $this->belongsToMany('App\Models\User', 'kelas_users', 'id_kelas', 'id_user');
  }

  public function prompt()
  {
  	return $this->hasMany('App\Models\Prompt','id_kelas','id');
  }

}
