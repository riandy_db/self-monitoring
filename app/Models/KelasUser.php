<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KelasUser extends Model
{
	protected $table = 'kelas_users';

	public $timestamps = false;

  protected $fillable = [
  	'id_kelas', 'id_user'
  ];
}
