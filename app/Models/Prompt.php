<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prompt extends Model
{

	protected $table = 'prompt';

	protected $fillable = [
  	'id_kelas', 'topik', 'pertanyaan'
  ];

	public function kelas(){
		return $this->belongsTo('App\Models\Kelas','id_kelas','id');
	}

	public function kataKunci(){
		return $this->hasMany('App\Models\KataKunciPrompt','id_prompt','id');
	}

	public function feedbackDefault(){
		return $this->hasMany('App\Models\Feedback','id_prompt','id');
	}

	public function jawaban(){
		return $this->hasMany('App\Models\Jawaban','id_prompt','id');
	}

}
