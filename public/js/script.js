$( document ).ready(function() {
// add class active for sidebar tab

	$.expr[":"].contains = $.expr.createPseudo(function(arg) {
	    return function( elem ) {
	        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
	    };
	});

	var pageTitle = $('div.wrapper').data('page');
	$("ul.side-nav li").find( ":contains('"+pageTitle+"')" ).parent().addClass("active");

	var pageActive = $('div.container').data('page');
	$("ul.navbar-right li").find( ":contains('"+pageActive+"')" ).parent().addClass("active");
});
