@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('title','| Tambah Pengguna')

@section('content')
<div class="wrapper container" data-page="{{ $role }}">
	<h2>Tambah <span class="capitalize">{{ $role }}</span></h2>

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
	@endif

	@if($role == 'instruktur')
	{!! Form::open(array('route' => 'admin.instruktur.store')) !!}
	@else
	{!! Form::open(array('route' => 'admin.mahasiswa.store')) !!}
	@endif

		{!! Form::hidden('role', $role) !!}

		@if ($role == 'mahasiswa')
			<div class="form-group">
			    {!! Form::label('no_identitas', 'Identitas:', ['class' => 'control-label']) !!}
			    {!! Form::text('no_identitas', null, ['class' => 'form-control', 'placeholder' => '1234567890']) !!}
			</div>
		@endif

		<div class="form-group">
		    {!! Form::label('name', 'Nama:', ['class' => 'control-label']) !!}
		    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Foo Bar']) !!}
		</div>
		
		<div class="form-group">
		    {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
		    {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'foo.bar']) !!}
		</div>
		
		<div class="form-group">
		    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
		    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'foo@bar.com']) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
		    {!! Form::password('password', array('class' => 'form-control')) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('password_confirmation', 'Repeat Password:', ['class' => 'control-label']) !!}
		    {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
		</div>

		<button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Tambah Pengguna</button>

		<a href="{{ $role == 'instruktur' ? route('admin.instruktur.index') : route('admin.mahasiswa.index') }}" class="btn btn-danger"><i class="fa fa-times"></i> Kembali</a>

	{!! Form::close() !!}

</div>
@stop