@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('title','| Ubah Kelas')

@section('content')
<div class="wrapper container" data-page="Kelas">
	<h2>Ubah <span class="capitalize">Kelas</span></h2>

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
	@endif

	{!! Form::model($data, [
	    'method' => 'PATCH',
	    'route' => ['admin.kelas.update', $data->id]
	]) !!}

		<div class="form-group">
		    {!! Form::label('nama_kelas', 'Nama Kelas:', ['class' => 'control-label']) !!}
		    {!! Form::text('nama_kelas', null, ['class' => 'form-control', 'placeholder' => 'Kalkulus 2']) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('tahun_ajar_mulai', 'Tahun Ajar:', ['class' => 'control-label']) !!}
		    {!! Form::text('tahun_ajar_mulai', null, ['class' => 'form-control', 'placeholder' => '2015']); !!}
		</div>

		<div class="form-group">
		    {!! Form::label('batch', 'Batch:', ['class' => 'control-label']) !!}
		    {!! Form::select('batch', ['ganjil' => 'Ganjil', 'genap' => 'Genap'], null, ['class' => 'form-control']); !!}
		</div>

		<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>

		<a href="{{ route('admin.kelas.index') }}" class="btn btn-danger"><i class="fa fa-times"></i> Kembali</a>

	{!! Form::close() !!}

</div>
@stop