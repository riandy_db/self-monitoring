@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('title','| Ubah Pengguna')

@section('content')
<div class="wrapper container" data-page="{{ $data->role }}">
	<h2>Ubah <span class="capitalize">{{ $data->role }}</span></h2>

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
	@endif

	@if($data->role == 'instruktur')
	{!! Form::model($data, [
	    'method' => 'PATCH',
	    'route' => ['admin.instruktur.update', $data->id]
	]) !!}
	@else
	{!! Form::model($data, [
	    'method' => 'PATCH',
	    'route' => ['admin.mahasiswa.update', $data->id]
	]) !!}
	@endif

		@if ($data->role == 'mahasiswa')
			<div class="form-group">
			    {!! Form::label('no_identitas', 'Identitas:', ['class' => 'control-label']) !!}
			    {!! Form::text('no_identitas', null, ['class' => 'form-control', 'placeholder' => '1234567890']) !!}
			</div>
		@endif

		<div class="form-group">
		    {!! Form::label('name', 'Nama:', ['class' => 'control-label']) !!}
		    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Foo Bar']) !!}
		</div>
		
		<div class="form-group">
		    {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
		    {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'foo.bar', 'disabled']) !!}
		</div>
		
		<div class="form-group">
		    {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
		    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'foo@bar.com']) !!}
		</div>

		<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>

		<a href="{{ $data->role == 'instruktur' ? route('admin.instruktur.index') : route('admin.mahasiswa.index') }}" class="btn btn-danger"><i class="fa fa-times"></i> Kembali</a>

	{!! Form::close() !!}

</div>
@stop