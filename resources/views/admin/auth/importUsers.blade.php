@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('title','| Import Mahasiswa')

@section('content')
<div class="wrapper container" data-page="Mahasiswa">
	<h2>Import <span class="capitalize">Mahasiswa</span></h2>

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
	@endif
	<p>Untuk melakukan import data mahasiswa, pastikan Anda menggunakan file dengan format .csv. Di dalam file tersebut pastikan data dimulai dari header yang berada pada row pertama. Format header: NPM | username | nama | password | email (opsional)</p>
	{!! Form::open(array('route' => 'admin.mahasiswa.storeImport', 'files' => true)) !!}
		
		<div class="form-group">
		    {!! Form::file('daftar_mahasiswa', ['accept' => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel']) !!}
		</div>

		<button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Import Mahasiswa</button>

		<a href="{{ route('admin.mahasiswa.index') }}" class="btn btn-danger"><i class="fa fa-times"></i> Kembali</a>

	{!! Form::close() !!}

</div>
@stop