@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('title','| Kelola Instruktur')

@section('content')
<div class="wrapper container" data-page="Instruktur">
  <h2>Daftar Instruktur Nonaktif</h2>

  @if(Session::has('flash_message'))
    <div class="alert alert-success">
      {{ Session::get('flash_message') }}
    </div>
  @endif
	<table id="example" class="table table-striped table-bordered" cellspacing="0" style="margin-top: 20px">
        <thead>
            <tr>
                <th>No</th>
                <th>Username</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($datas as $index => $data)
            <tr>
                <td>{{ ($datas->perPage()*($datas->currentPage()-1))+($index+1) }}</td>
                <td>{{ $data->username }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->email }}</td>
                <td>
                  <a href="{{ url('/admin/instruktur/activate/'.$data->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Aktifkan</a>
                </td>
            </tr>
         @endforeach
        </tbody>
    </table>
    {{ $datas->links() }}
</div>
@stop