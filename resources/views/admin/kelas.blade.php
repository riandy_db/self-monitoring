@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('title','| Kelola Kelas')

@section('content')
<div class="wrapper container" data-page="Kelas">
  <h2>Daftar Kelas</h2>
  <a href="{{ route('admin.kelas.create') }}" class="btn btn-success pull-right btn-add btn-green"><i class="fa fa-plus"></i> Tambah Kelas</a>
  @if(Session::has('flash_message'))
    <div class="alert alert-success">
      {{ Session::get('flash_message') }}
    </div>
  @endif
  <table id="example" class="table table-striped table-bordered" cellspacing="0" style="margin-top: 20px">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Kelas</th>
            <th>Tahun Ajar</th>
            <th>Batch</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($datas as $index => $data)
      <tr>
        <td>{{ ($datas->perPage()*($datas->currentPage()-1))+($index+1) }}</td>
        <td>{{ $data->nama_kelas }}</td>
        <td>{{ $data->tahun_ajar_mulai }}</td>
        <td><span class="capitalize">{{ $data->batch }}</span></td>
        <td>
          {!! Form::open([
            'method' => 'DELETE',
            'route' => ['admin.kelas.destroy', $data->id]
          ]) !!}
          <a href="{{ route('admin.kelas.edit', $data->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil fa-fw"></i> Ubah</a>
          {!! Form::button('<i class="fa fa-trash"></i> Hapus', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] ) !!}
          {!! Form::close() !!}
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  {{ $datas->links() }}
</div>
@stop