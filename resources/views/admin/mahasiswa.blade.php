@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('title','| Kelola Mahasiswa')

@section('content')
<div class="wrapper container" data-page="Mahasiswa">

  <h2>Daftar Mahasiswa</h2>
  <a href="{{ route('admin.mahasiswa.create') }}" class="btn btn-success pull-right btn-add btn-green"><i class="fa fa-plus"></i> Tambah</a>
  <a href="{{ route('admin.mahasiswa.import') }}" class="btn btn-success pull-right btn-add btn-green" style="margin-right: 5px"><i class="fa fa-upload"></i> Import</a>
  @if(Session::has('flash_message'))
  <div class="alert alert-success">
    {{ Session::get('flash_message') }}
  </div>
  @endif
	<table id="example" class="table table-striped table-bordered" cellspacing="0" style="margin-top: 20px">
        <thead>
            <tr>
                <th>No</th>
                <th>Identitas</th>
                <th>Username</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($datas as $index => $data)
            <tr>
                <td>{{ ($datas->perPage()*($datas->currentPage()-1))+($index+1) }}</td>
                <td>{{ $data->no_identitas }}</td>
                <td>{{ $data->username }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->email }}</td>
                <td>
                  {!! Form::open([
                    'method' => 'DELETE',
                    'route' => ['admin.mahasiswa.destroy', $data->id]
                  ]) !!}
                  <a href="{{ route('admin.mahasiswa.edit', $data->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil fa-fw"></i> Ubah</a>
                  {!! Form::button('<i class="fa fa-trash"></i> Hapus', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] ) !!}
                  {!! Form::close() !!}
                </td>
            </tr>
         @endforeach
        </tbody>
    </table>
    {{ $datas->links() }}
</div>
@stop