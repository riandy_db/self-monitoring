@section('sidebar')
@can('admin-access')
<div class="nav-side-menu">
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
        <div class="menu-list">
            <ul id="menu-content" class="menu-content collapse out side-nav">
				<li>
					<a href="{{ route('admin.index') }}"><span class="fa fa-fw fa-dashboard"></span> Dashboard</a>
				</li>
				<li>
					<a href="{{ route('admin.instruktur.index') }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Instruktur</a>
				</li>
				<li>
					<a href="{{ route('admin.mahasiswa.index') }}"><span class="fa fa-users" aria-hidden="true"></span> Mahasiswa</a>
				</li>
				<li>
					<a href="/admin/kelas"><span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span> Kelas</a>
				</li>
{{--
                <li  data-toggle="collapse" data-target="#products" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> UI Elements <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="products">
                    <li class="active"><a href="#">CSS3 Animation</a></li>
                    <li><a href="#">General</a></li>
                    <li><a href="#">Buttons</a></li>
                    <li><a href="#">Tabs & Accordions</a></li>
                    <li><a href="#">Typography</a></li>
                    <li><a href="#">FontAwesome</a></li>
                    <li><a href="#">Slider</a></li>
                    <li><a href="#">Panels</a></li>
                    <li><a href="#">Widgets</a></li>
                    <li><a href="#">Bootstrap Model</a></li>
                </ul>


                <li data-toggle="collapse" data-target="#service" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Services <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="service">
                  <li>New Service 1</li>
                  <li>New Service 2</li>
                  <li>New Service 3</li>
                </ul>


                <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> New <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="new">
                  <li>New New 1</li>
                  <li>New New 2</li>
                  <li>New New 3</li>
                </ul>


                 <li>
                  <a href="#">
                  <i class="fa fa-user fa-lg"></i> Profile
                  </a>
                  </li>

                 <li>
                  <a href="#">
                  <i class="fa fa-users fa-lg"></i> Users
                  </a>
                </li>
--}}
            </ul>
     </div>
</div>

{{--
<ul class="nav navbar-nav side-nav">
	<li>
		<a href="/home/"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
	</li>
	<li>
		<a href="/admin/instruktur"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Instruktur</a>
	</li>
	<li>
		<a href="/admin/mahasiswa"><span class="fa fa-users" aria-hidden="true"></span> Mahasiswa</a>
	</li>
	<li>
		<a href="/admin/kelas"><span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span> Kelas</a>
	</li>

	<li>
		<a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
		<ul id="demo" class="collapse">
			<li>
				<a href="#">Dropdown Item</a>
			</li>
			<li>
				<a href="#">Dropdown Item</a>
			</li>
		</ul>
	</li>
--}}
</ul>
@endcan
@stop