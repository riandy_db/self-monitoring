@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
<!--             @if ($errors->has('oldPassword'))
                <span class="help-block">
                    <strong>{{ $errors->first('no-match') }}</strong>
                </span>
            @endif -->
            @if($errors->any())
            <div class="alert alert-danger" style="margin-top: 20px">
                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            <div class="panel panel-login">

                <div class="panel-heading">Ubah Foto Avatar</div>
                <div class="panel-body">
                  <div class="alert alert-warning small">
                    <ul>
                      <li>Gunakan foto dengan rasio lebar dan tinggi 1:1 untuk hasil terbaik.</li>
                      <li>Ekstensi file foto yang diterima yaitu .jpg, .jpeg dan .png.</li>
                      <li>Ukuran file foto maksimal 2 MB.</li>
                    </ul>
                  </div>
                  <form enctype="multipart/form-data" action="change-avatar/submit" method="POST">
                      {{ csrf_field() }}
                      <input type="file" name="avatar" accept="image/*">
                      <a role="button" class="pull-right btn btn-sm btn-danger" style="margin-left: 5px" href="/"><i class="fa fa-times"></i> Batalkan</a>
                      <button type="submit" class="pull-right btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
