@extends('layouts.app')

@section('content')
@section('title', ' | Login')



    <header>    
        <div class="col-md-4 col-md-offset-7">
            <div class="panel panel-login">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login.custom') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                @if (Session::has('flash_message'))
                                    <span class="help-block" style="color: #a94442;">
                                        <strong>{{ Session::get('flash_message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-green">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>

                                <!--a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end login -->
            
       
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h1 class="section-heading">Self-Monitoring Online</h1>
                    <hr class="light">
                    <p align="left">
                        Self-Monitoring Online adalah sebuah aplikasi berbasis <i style="font-style: italic;">website</i> yang mengimplementasikan intervensi 
                        dalam bentuk metode instruksional dalam perkuliahan. Dengan menggunakan aplikasi 
                        Self-Monitoring Online, pengajar dapat memantau performa perkuliahan mahasiswa dan memperbaiki 
                        pemahaman mahasiswa atas sebuah konsep dalam materi kuliah secara personal melalui 
                        diskusi secara asinkronus yang kondusif.
                    </p>
                    <p>Klik tombol dibawah ini untuk melihat penjelasan lebih lengkap</p>
                    <a href="{{ url('/description') }}" class="page-scroll btn btn-default btn-xl sr-button">Deskripsi</a>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h1 class="section-heading">Kontak</h1>
                    <hr class="primary">
                    <p>Ingin memberi masukan? atau punya kendala dalam menggunakan sistem ini? Silahkan menghubungi kami ke alamat berikut.</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-map-marker fa-3x sr-contact"></i>
                    <p><a href="http://dl2.cs.ui.ac.id/" style="color:black;">Laboratorium Digital Library & Distance Learning
                        Fakultas Ilmu Komputer Universitas Indonesia
                        Kampus Baru UI Depok, Jawa Barat, 16424</a>
                    </p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:harrybs@cs.ui.ac.id" style="color:black;">Harry B. Santoso, PhD <br/> harrybs@cs.ui.ac.id</a></p>
                </div>
            </div>
        </div>
    </section>


<!-- modal registrasi atau login -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title">Selamat!</h2>
      </div>
      <div class="modal-body">
        @if (Session::get('status')=='mahasiswa')
            <p>Registrasi anda berhasil. Silahkan login menggunakan akun anda.</p>
        @endif

        @if (Session::get('status')=='instruktur')
            <p>Registrasi anda berhasil. Silahkan hubungi administrator untuk mengaktifkan akun anda.</p>
        @endif
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection

@section('script')
<script>
$( "body" ).addClass( "home-login" );
</script>

@if (session('status'))
<script type="text/javascript">
    $(document).ready(function () {
        $('#myModal').modal('show');
    });
</script>
@endif

@endsection
