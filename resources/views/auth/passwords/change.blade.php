@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
<!--             @if ($errors->has('oldPassword'))
                <span class="help-block">
                    <strong>{{ $errors->first('no-match') }}</strong>
                </span>
            @endif -->
           @if(Session::has('flash_error'))
              <div class="alert alert-danger" style="margin-top: 20px">
                  <strong>{{ Session::get('flash_error') }}</strong>
              </div>
            @endif
            <div class="panel panel-login">
 
                <div class="panel-heading">Ubah Password</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('set-password') }}">
                        {{ csrf_field() }}

                         <div class="form-group{{ $errors->has('oldPassword') ? ' has-error' : '' }}">
                            <label for="oldPassword" class="col-md-4 control-label">Old Password</label>

                            <div class="col-md-6">
                                <input id="oldPassword" type="password" class="form-control" name="oldPassword">

                                @if ($errors->has('oldPassword'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('oldPassword') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">New Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm New Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i> Ubah Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
