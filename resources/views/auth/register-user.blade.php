@extends('layouts.app')

@section('content')

</br>
</br>

<div class="container">
	<div class="row">
		
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<h1 class="section-heading">Registrasi</h1>
			<hr class="primary">
			<p>Silahkan pilih role yang sesuai dengan anda!</p>
		</div>
		<a href="{{ url('/register/mahasiswa') }}">
			<div class="col-lg-4 col-lg-offset-2 text-center">
				<img src="{{URL::asset('/img/student (1).png')}}" alt="profile Pic" height="150" width="150">
				<h2 style="color:black;">Siswa/Mahasiswa</h2>
			</div>
		</a>

		<a href="{{ url('/register/instruktur') }}">
			<div class="col-lg-4 text-center">
				<img src="{{URL::asset('/img/teacher1.png')}}" alt="profile Pic" height="150" width="150">
				<h2 style="color:black;">Instruktur</h2>
			</div>
		</a>
	</div>
</div>

</br>
</br>
</br>

@endsection
