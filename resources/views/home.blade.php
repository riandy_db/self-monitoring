@extends('layouts.app')
@extends('admin.sidebar-admin')

@section('content')

    @can('admin-access')

        @section('title','Dashboard Admin')

        <div class="wrapper admin" data-page="Dashboard">
            <div class="content">
                <p>Selamat Datang {{ Auth::user()->name }}</p>
                <p class='text-smaller'>Saat ini Anda berada di Dashboard Admin</p>
                <p class='text-smaller'>Silakan klik menu di samping kiri untuk memulai</p>
            </div>
        </div>

    @endcan

@endsection
