@extends('layouts.app')

@section('content')
	@section('title', ' | Tambah Mahasiswa')
	
	<div class="container" data-page="Kelas">
		<div class="row">
			<div class="large-panel col-md-12">
				<div class="page-info">
					<div class="page-info__title">{{$kelasInfo->nama_kelas}}</div>
					<div class="page-info__kelas">{{$kelasInfo->tahun_ajar_mulai}} {{$kelasInfo->batch}}</div>
				</div>
				@if(Session::has('flash_message'))
				    <div class="alert alert-success">
				        {{ Session::get('flash_message') }}
				    </div>
				@endif

				@if($errors->any())
			    <div class="alert alert-danger">
			        @foreach($errors->all() as $error)
			            <p>{{ $error }}</p>
			        @endforeach
			    </div>
				@endif
				
				@if(count($userAvailable) == 0)
					<div class="border">
						<h2 class="text-center">Tidak ada mahasiswa yang dapat ditambahkan ke kelas ini</h2>
					</div>
					<a href="{{ route('instruktur.kelas.show', $kelasInfo->id) }}" class="btn btn-default pull-right" style="margin-top: 5px"><i class="fa fa-chevron-left"></i> Kembali</a>
				@else
				{!! Form::open(array('route' => array ('instruktur.mahasiswa.store', $kelasInfo->id))) !!}

					{!! Form::hidden('nama_kelas', $kelasInfo->id) !!}
					<div class="form-group">
					    {!! Form::label('daftar_mahasiswa', 'Tambahkan Mahasiswa:', ['class' => 'control-label']) !!}
					    {!! Form::select('daftar_mahasiswa[]', $userAvailable, null, ['class' => 'form-control select-select2 select-textarea select2-daftar_mahasiswa', 'multiple' => 'multiple']); !!}
					</div>

					<button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Tambah Mahasiswa</button>

					<a href="{{ route('instruktur.kelas.show', $kelasInfo->id) }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>

				{!! Form::close() !!}
				@endif
			</div>
		</div>
	</div>
@endsection

@section('script')
  {{-- import style2 script --}}
  <script src="{{asset('js/select2/select2.min.js')}}"></script>

  <script type="text/javascript">
		$(document).ready(function() {
			$.fn.select2.defaults.set("theme", "bootstrap");
		  $(".select-select2").select2();
		  $(".select2-daftar_mahasiswa").select2({
		  	allowClear: true,
		  	placeholder: "Pilih mahasiswa",
		  });
		});
	</script>
@endsection