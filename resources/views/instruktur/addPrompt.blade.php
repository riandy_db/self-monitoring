@extends('layouts.app')
@section('content')
	@section('title', ' | Tambah Prompt')
	<br/>
	<div class="container" data-page="Prompt">
		<div class="row">
			<div class="large-panel col-md-12">
			<h2>Tambah Prompt</h2>
			<br/>
			@if(Session::has('flash_message'))
			    <div class="alert alert-success">
			        {{ Session::get('flash_message') }}
			    </div>
			@endif
			@if($errors->any())
		    <div class="alert alert-danger">
		        @foreach($errors->all() as $error)
		            <p>{{ $error }}</p>
		        @endforeach
		    </div>
			@endif
				{!! Form::open(array('route' => 'instruktur.prompt.store')) !!}

					<div class="form-group">
				    {!! Form::label('id_kelas', 'Kelas:', ['class' => 'control-label']) !!}
				    {!! Form::select('id_kelas', $daftarKelas, null, ['class' => 'form-control select-select2']) !!}
					</div>

					<div class="form-group">
			    {!! Form::label('topik', 'Topik:', ['class' => 'control-label']) !!}
			    {!! Form::text('topik', null, ['class' => 'form-control', 'placeholder' => 'Tulis topik']) !!}
					</div>

					<div class="form-group">
			    {!! Form::label('pertanyaan', 'Pertanyaan:', ['class' => 'control-label']) !!}
			    {!! Form::textarea('pertanyaan', null, ['class' => 'form-control', 'placeholder' => 'Tulis pertanyaan prompt', 'rows' => 3]) !!}
					</div>

					<div class="form-group">
			    {!! Form::label('kata_kunci', 'Kata Kunci:', ['class' => 'control-label']) !!}
			    {!! Form::text('kata_kunci', null, ['class' => 'form-control', 'placeholder' => 'Tulis kata kunci prompt, pisahkan dengan titik koma (;)']) !!}
					</div>

					<div class="form-group">
			    {!! Form::label('feedback_default', 'Feedback Default:', ['class' => 'control-label']) !!}
			    {!! Form::textarea('feedback_default', null, ['class' => 'form-control', 'placeholder' => 'Tulis feedback yang mungkin Anda berikan, pisahkan dengan titik koma (;)', 'rows' => 3]) !!}
					</div>				

					<button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Tambah Prompt</button>

					<a href="{{ route('instruktur.prompt.index') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection

@section('script')
<script src="{{asset('js/select2/select2.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$.fn.select2.defaults.set("theme", "bootstrap");
	  $(".select-select2").select2();
	  $("#chkbox-balasan").click(function (){
	  	$("#chkbox-balasan-value").prop("checked", !$("#chkbox-balasan-value").prop("checked"));
	  });
	});
</script>
@endsection