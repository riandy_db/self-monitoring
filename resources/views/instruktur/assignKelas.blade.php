@extends('layouts.app')

@section('content')

	@section('title','| Tambah Kelas')
	<br/>
	<br/>
	<div class="container" data-page="Kelas">
		
		<div class="row">
			<div class="large-panel col-md-12">
				<h2>Tambah Kelas</h2>
				<br/>
				@if(Session::has('flash_message'))
				    <div class="alert alert-success">
				        {{ Session::get('flash_message') }}
				    </div>
				@endif


				{!! Form::open(array('route' => 'instruktur.kelas.store')) !!}

					<div class="form-group">
					    {!! Form::label('nama_kelas', 'Nama Kelas:', ['class' => 'control-label']) !!}
					    {!! Form::select('nama_kelas', $kelas, null, ['class' => 'form-control select-select2']); !!}
					</div>

					<div class="form-group">
					    {!! Form::label('daftar_mahasiswa', 'Daftar Mahasiswa:', ['class' => 'control-label']) !!}
					    {!! Form::select('daftar_mahasiswa[]', $mahasiswa, null, ['class' => 'form-control select-select2 select-textarea select2-daftar_mahasiswa', 'multiple' => 'multiple']); !!}
					</div>

					<button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Tambah Kelas</button>

					<a href="{{ route('instruktur.kelas.index') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>

				{!! Form::close() !!}
				</div>
		</div>
	</div>

@endsection

@section('script')
  {{-- import style2 script --}}
  <script src="{{asset('js/select2/select2.min.js')}}"></script>

  <script type="text/javascript">
		$(document).ready(function() {
			$.fn.select2.defaults.set("theme", "bootstrap");
		  $(".select-select2").select2();
		  $(".select2-daftar_mahasiswa").select2({
		  	allowClear: true,
		  	placeholder: "Pilih mahasiswa"
		  });
		});
	</script>
@endsection
