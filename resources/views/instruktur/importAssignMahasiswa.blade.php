@extends('layouts.app')
@section('content')
	@section('title', ' | Import Mahasiswa')
	<br/>
	<div class="container" data-page="Kelas">
		<div class="row">
			<div class="large-panel col-md-12">
				<h2>Import Mahasiswa Kelas {{$kelas->nama_kelas}}</h2>
				<br/>
			@if(Session::has('flash_message'))
			    <div class="alert alert-success">
			        {{ Session::get('flash_message') }}
			    </div>
			@endif
			@if($errors->any())
		    <div class="alert alert-danger">
		        @foreach($errors->all() as $error)
		            <p>{{ $error }}</p>
		        @endforeach
		    </div>
			@endif
			<p>Untuk melakukan import data mahasiswa, pastikan Anda menggunakan file dengan format .csv. Di dalam file tersebut pastikan data dimulai dari header yang berada pada row pertama. Pastikan mahasiswa telah didaftarkan sebelumnya oleh administrator. Format header: NPM | username </p>
			<br>
			{!! Form::open(array('route' => 'instruktur.kelas.storeImport', 'files' => true)) !!}
				{!! Form::hidden('id_kelas', $kelas->id) !!}
				<div class="form-group">
						{!! Form::label('daftar_mahasiswa', 'File Daftar Mahasiswa:', ['class' => 'control-label']) !!}
				    {!! Form::file('daftar_mahasiswa', ['accept' => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel']) !!}
				</div>

				<button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Import Mahasiswa</button>

				<a href="{{ url('instruktur/kelas/' . $kelas->id) }}" class="btn btn-primary"><i class="fa fa-times"></i> Kembali</a>

			{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection

@section('script')
<script src="{{asset('js/select2/select2.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$.fn.select2.defaults.set("theme", "bootstrap");
	  $(".select-select2").select2();
	  $("#chkbox-balasan").click(function (){
	  	$("#chkbox-balasan-value").prop("checked", !$("#chkbox-balasan-value").prop("checked"));
	  });
	});
</script>
@endsection