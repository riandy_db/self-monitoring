@extends('layouts.app')

@section('content')

	@section('title','| Daftar Kelas')
	<div class="container" data-page="Kelas">
		<div class="row">
			<div class="large-panel col-md-12">
				<div class="page-info">
					<div class="page-info__title">Daftar Kelas</div>
					<div class="page-info__description">Kelas yang dikelola oleh {{ Auth::user()->name }}</div>	
				</div>
				@if(Session::has('flash_message'))
			    <div class="alert alert-success">
			        {{ Session::get('flash_message') }}
			    </div>
				@endif
				<div class="panel__button">
					<a href="{{url('instruktur/kelas/assign')}}" class="btn btn-success btn-green"><i class="fa fa-plus"></i> Tambah Kelas</a>
				</div>
				<div class="border">
				@if(count($datas)==0)
					<h2 class="text-center">Anda belum mengelola kelas apapun</h2>
				@else
					@foreach($datas as $index => $data)
					@if($index % 3 == 0)
						<div class="row">
					@endif
							<div class="card-panel col-sm-4">
								<a href="{{url('instruktur/kelas/'.$data->id)}}">
									<div class="kelas_img">
										<div class="card-panel__overlay">
											<div class="card-panel__overlay-button">
												{!! Form::open([
												    'method' => 'POST', 
												    'route' => ['destroy-kelas', Auth::user()->id, $data->id],
													'onsubmit' => 'return ConfirmDelete()'])
												!!}
												{!! Form::hidden('id_prompt', $data->id_prompt) !!}
							          {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] ) !!}
							          {!! Form::close() !!}
											</div>
											<div class="card-panel__overlay-text">
												<div class="kelas_description">
													<i class="fa fa-info-circle"></i> Lihat Rincian
													<!-- <div class="kelas_description--mahasiswa">
														Total Mahasiswa: 
													</div> -->
													<!-- <div class="kelas_description--prompt">
														Jumlah Prompt: 5 
													</div> -->
												</div>
											</div>
										</div>
										<img class="img-responsive" src="{{asset('img/classroom.jpg')}}" alt="">
									</div>
									<div class="kelas_title">
										<h2>{{$data->nama_kelas}}</h2>
										<small>{{$data->tahun_ajar_mulai}} <span class="capitalize">{{$data->batch}}</span></small>
									</div>
								</a>
							</div>
					@if($index % 3 == 2)
						</div>
					@endif
					@endforeach
				@endif
				</div>
			</div>
		</div>
	</div>

@endsection

@section('script')
<script>

  function ConfirmDelete()
  {
  var x = confirm("Are you sure you want to delete?");
  if (x)
    return true;
  else
    return false;
  }

</script>	
@endsection