@extends('layouts.app')

@section('content')

	@section('title','| Daftar Prompt')
	<div class="container" data-page="Prompt">
		<div class="row">
			<div class="large-panel col-md-12">
				<div class="page-info">
					<div class="page-info__title">Daftar Prompt</div>
					<div class="page-info__description">Prompt yang telah dibuat oleh para instruktur</div>	
				</div>
				@if(Session::has('flash_message'))
			    <div class="alert alert-success">
			        {{ Session::get('flash_message') }}
			    </div>
				@endif
				<div class="panel__button">
					<a href="{{ route('instruktur.prompt.create') }}" class="btn btn-success btn-green"><i class="fa fa-plus"></i> Tambah Prompt</a>
				</div>
				
				<div class="row">
					<div class="col-md-3">
						<div class="panel--sidebar">	
							<table id="example" class="table table-striped table-kelas" cellspacing="0" style="margin-bottom: 20px">
						    <thead>
						        <tr>
						            <th class="text-center">Kelas</th>
						        </tr>
						    </thead>
						    <tbody id="daftarKelas">
						    @if(count($daftarKelas) == 0)
									<tr>
										<td class="text-center"><h2 style="margin: 20px 0px">Anda belum mengelola kelas</h2></td>
									</tr>
						    @else
						    @foreach($daftarKelas as $index => $kelas)
						    	<tr>
						    		<td id="k{{$kelas->id}}" class="item"><a href="{{ route('show-prompts', $kelas->id) }}">{{$kelas->nama_kelas}} <small class="capitalize">({{$kelas->tahun_ajar_mulai}} {{$kelas->batch}})</small></a></td>
						    	</tr>
						    @endforeach
						    @endif
<!-- 						    	<tr>
						    		<td class="active-kelas"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Logika Komputer</a></td>
						    	</tr> -->
						    </tbody>
						  </table>
						</div>
					</div>
					<div class="col-md-9">
						<div class="panel--main-prompt">
							<table id="example" class="table table-striped table-kelas" cellspacing="0">
						    <thead>
						        <tr>
						            <th class="text-center">Prompt</th>
						        </tr>
						    </thead>
						    <tbody>
						    @if(count($daftarPrompt) == 0)
									<tr>
										<td class="text-center"><h2 style="margin: 20px 0px">Belum terdapat prompt</h2></td>
									</tr>
						    @else
					    	@foreach($daftarPrompt as $index => $prompt)
					    		<tr>
					    			<td class="item"><a href="{{ route('show.single.prompt', ['id_kelas' => $prompt->id_kelas, 'id_prompt' => $prompt->id]) }}">{{$prompt->pertanyaan}}</a></td>
					    		</tr>
					    	@endforeach
					    	@endif
						    	<!-- <tr>
						    		<td class="prompt-question"><a href="#">Apa itu persona?</a></td>
						    	</tr> -->
						    </tbody>
						  </table>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		if({{$idKelas}} == 0){
			$('#daftarKelas').find('td:first').addClass('active-kelas');
			$('.active-kelas a').prepend('<i class="fa fa-caret-right" aria-hidden="true"></i> ');
		} else {
			$('td#k{{$idKelas}}').addClass('active-kelas');
			$('.active-kelas a').prepend('<i class="fa fa-caret-right" aria-hidden="true"></i> ');
		}
	});
</script>
@endsection