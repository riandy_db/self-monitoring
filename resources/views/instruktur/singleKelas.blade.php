@extends('layouts.app')

@section('content')
	@section('title', '| Kelas ' . $kelasInfo->nama_kelas)
	<div class="container" data-page="Kelas">
		<div class="row">
			<div class="large-panel col-md-12">
				<div class="page-info">
					<div class="page-info__title">{{$kelasInfo->nama_kelas}}</div>
					<div class="page-info__kelas">{{$kelasInfo->tahun_ajar_mulai}} {{$kelasInfo->batch}}</div>
				</div>
				@if(Session::has('flash_message'))
			    <div class="alert alert-success">
			        {{ Session::get('flash_message') }}
			    </div>
				@endif
				<div class="panel__button">
				<a href="{{ route('instruktur.kelas.index') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
					<a href="{{url('instruktur/kelas/' . $kelasInfo->id . '/tambah-mahasiswa')}}" class="btn btn-success btn-green"><i class="fa fa-plus"></i> Tambah</a>
					<a href="{{url('instruktur/kelas/' . $kelasInfo->id . '/import-mahasiswa')}}" class="btn btn-success btn-green" style="margin-right: 5px"><i class="fa fa-upload"></i> Import</a>					
				</div>
				@if(count($userKelas)==0)
				<div class="border">
					<h2 class="text-center">Kelas ini masih kosong</h2>
				</div>
				@else
				<table id="example" class="table table-striped table-bordered" cellspacing="0">
			    <thead>
			        <tr>
			            <th>No</th>
			            <th>No Identitas</th>
			            <th>Nama Mahasiswa</th>
			            <th>Email</th>
			            <th>Action</th>
			        </tr>
			    </thead>
			    <tbody>
			    @foreach($userKelas as $index => $data)
			      <tr>
			        <td>{{ ($userKelas->perPage()*($userKelas->currentPage()-1))+($index+1) }}</td>
			        <td>{{ $data->no_identitas }}</td>
			        <td>{{ $data->name }}</td>
			        <td>{{ $data->email }}</td>
			        <td>
								{!! Form::open([
								    'method' => 'POST', 
								    'route' => ['destroy-mahasiswa', $data->id, $kelasInfo->id]]) !!}
			          {!! Form::button('<i class="fa fa-trash"></i> Hapus', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] ) !!}
			          {!! Form::close() !!}
			        </td>
			      </tr>
			     @endforeach
			    </tbody>
			  </table>
			  @endif
			  {{ $userKelas->links() }}
			</div>
		</div>
	</div>
	

@endsection
