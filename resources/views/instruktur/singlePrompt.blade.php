@extends('layouts.app')

@section('content')
	@section('title', ' | Prompt ')

	<div class="container" data-page="Prompt">
		<div class="row">
			<div class="large-panel col-md-10 col-md-offset-1">
				<div class="kelas-prompt">{{$kelasInfo->nama_kelas}} <small>({{$kelasInfo->tahun_ajar_mulai}} {{$kelasInfo->batch}})</small> </div>
				<div class="prompt-topik">Topik: {{$prompt->topik}}</div>
				@if(Session::has('flash_message'))
			    <div class="alert alert-success">
			        {{ Session::get('flash_message') }}
			    </div>
				@endif
				@if($errors->any())
			    <div class="alert alert-danger">
			        @foreach($errors->all() as $error)
			            <p>{{ $error }}</p>
			        @endforeach
			    </div>
				@endif
				<div class="panel__button">
					<a href="{{ route('show-prompts', $kelasInfo->id) }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
				</div>
				<div class="border">
				<div class="panel-prompt">
					<div class="panel-prompt__button">
						{!! Form::open(array('route' => 'destroy-prompt')) !!}
						{!! Form::hidden('id_prompt', $prompt->id) !!}
	          {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] ) !!}
	          {!! Form::close() !!}
					</div>
					<div class="panel-prompt__question">
						{{$prompt->pertanyaan}}
					</div>
					<div class="panel-prompt__kata-kunci">
						@if(count($kataKunci) == 0)
						Tidak ada kata kunci untuk prompt ini.
						@else
						Kata kunci: @foreach($kataKunci as $index => $data)
													{{$data->katakunci}}@if($index!=count($kataKunci)-1),@endif
												@endforeach
						@endif
					</div>
					<div class="panel-prompt__details">
						Created on @datetime($prompt->created_at)
					</div>
				</div>
				<div class="panel-answers">
					@foreach($jawabanPrompt as $key => $value)
					<div class="panel-answer--single">
						<div class="panel-answer--single__feedback">
						@if($feedbackInstruktur[$key] == null)
							<button id="btn{{$key}}" value="{{$key}}" class="btn btn-primary btn-sm btn-fdbck" type="button"><i class="fa fa-reply"></i></button>
						@endif
						</div>
						<div class="panel-answer--single__avatar">	
								<img src="{{ url('self-monitoring/public/img/avatar/') }}/{{ $jawabanUser[$key]->url_foto }}" alt="">
						</div>					
						<div class="panel-answer--single__answer">
							{{$value->jawaban}}
						</div>
						<div class="panel-answer--single__user">
							<small>by	{{$jawabanUser[$key]->name}} ({{$jawabanUser[$key]->no_identitas}})</small>
						</div>
						<div class="panel-answer--single__details">
							Replied on @datetime($value->created_at)
						</div>
						<div class="panel-feedback">	
							{{$feedbackInstruktur[$key] != null && $feedbackInstruktur[$key]->feedback_default != null ? 'Feedback: ' .$feedbackInstruktur[$key]->feedback_default : ''}}
						</div>
						@if(count($feedbackJawaban) > $key && $feedbackJawaban[$key] != null)
						<div class="panel-feedback__replied">	
							Balasan: <br>	
							{{$feedbackJawaban[$key]}}
						</div>
						@endif
						<div id="ans{{$key}}" class="panel-answering hidden">	
						{!! Form::open(array('route' => 'store-feedback')) !!}
							{!! Form::hidden('id_jawaban', $value->id) !!}
							<div class="form-group">
							    {!! Form::label('feedback', 'Pilih feedback:', ['class' => 'control-label']) !!}
							    {!! Form::select('feedback', $feedbackPrompt, null, ['class' => 'form-control select-select2 select2-feedback']); !!}
							</div>

							<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Simpan Feedback</button>
							<button id="" value={{$key}} class="btn btn-danger btn-cancel" type="button"><i class="fa fa-times"></i> Batalkan</button>
							<!-- <a id="btn-cancel" class="btn btn-danger" href="#"><i class="fa fa-times"></i> Batalkan</a> -->

						{!! Form::close() !!}
						</div>						
					</div>
					@endforeach
				</div>
			</div>
			</div>
		</div>
	</div>

@endsection

@section('script')
  {{-- import style2 script --}}
  <script src="{{asset('js/select2/select2.min.js')}}"></script>

  <script type="text/javascript">
		$(document).ready(function() {
			$.fn.select2.defaults.set("theme", "bootstrap");
		  $(".select-select2").select2();
		  $(".select2-feedback").select2({
		  	allowClear: true,
		  	placeholder: "Pilih feedback"
		  });
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('.btn-fdbck').click(function(){
			  var id = this.value;
				$('#btn'+id).addClass('hidden');
		    $('#ans'+id).removeClass('hidden');
		    $('.select2-container').css('width','auto');
			});
			$('.btn-cancel').click(function(){
			  var id = this.value;
				$('#btn'+id).removeClass('hidden');
		    $('#ans'+id).addClass('hidden');
			});
		});
	</script>
@endsection
