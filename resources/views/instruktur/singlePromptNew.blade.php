@extends('layouts.app')

@section('content')
	@section('title', ' | Prompt ')

	<div class="container" data-page="Prompt">
		<div class="row">
			<div class="large-panel col-md-10 col-md-offset-1">
				<div class="kelas-prompt">{{$kelasInfo->nama_kelas}} <small>({{$kelasInfo->tahun_ajar_mulai}} {{$kelasInfo->batch}})</small> </div>
				<div class="prompt-topik">Topik: {{$prompt->topik}}</div>
				
				@if(Session::has('flash_message'))
				    <div class="alert alert-success">
				        {{ Session::get('flash_message') }}
				    </div>
				@endif
				@if($errors->any())
				    <div class="alert alert-danger">
				        @foreach($errors->all() as $error)
				            <p>{{ $error }}</p>
				        @endforeach
				    </div>
				@endif

				<div class="panel__button">
					<a href="{{ route('show-prompts', $kelasInfo->id) }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
				</div>
				
				<div class="border">
					<div class="panel-prompt">
						<div class="panel-prompt__button">
							{!! Form::open(array('route' => 'destroy-prompt')) !!}
							{!! Form::hidden('id_prompt', $prompt->id) !!}
					        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] ) !!}
					        {!! Form::close() !!}
						</div>
						<div class="panel-prompt__question">
							{{$prompt->pertanyaan}}
						</div>
						<div class="panel-prompt__kata-kunci">
							@if(count($kataKunci) == 0)
							Tidak ada kata kunci untuk prompt ini.
							@else
							Kata kunci: @foreach($kataKunci as $index => $data)
														{{$data->katakunci}}@if($index!=count($kataKunci)-1),@endif
													@endforeach
							@endif
						</div>
						<div class="panel-prompt__details">
							Created on @datetime($prompt->created_at)
						</div>
					</div>
					<br/>
					<h3>Balasan:</h3>
					<div class="panel-answers-user">
						@foreach($jawabanUser as $key => $value)
							
							<a href="{{ route('show.single.prompt.user', ['id_kelas' => $prompt->id_kelas, 'id_prompt' => $prompt->id, 'id_user' => $jawabanUser[$key]->id ]) }}">	
								<div id="list-user" class="panel-answer--single">
									<div class="panel-answer--single__avatar">	
										<img src="{{ url('/self-monitoring/public/img/avatar/') }}/{{ $jawabanUser[$key]->url_foto }}" alt="">
									</div>
									<div class="panel-answer--single__user">
										<h3><strong>{{$jawabanUser[$key]->name}} ({{$jawabanUser[$key]->no_identitas}})</strong></h3>
									</div>							
													
								</div>
							</a>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('script')
  {{-- import style2 script --}}
@endsection
