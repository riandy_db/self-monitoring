@extends('layouts.app')

@section('content')
	@section('title', ' | Prompt ')

	<div class="container" data-page="Prompt">
		<div class="row">
			<div class="large-panel col-md-10 col-md-offset-1">
				<div class="kelas-prompt">{{$kelasInfo->nama_kelas}} <small>({{$kelasInfo->tahun_ajar_mulai}} {{$kelasInfo->batch}})</small> </div>
				<div class="prompt-topik">Topik: {{$prompt->topik}}</div>
				@if(Session::has('flash_message'))
				    <div class="alert alert-success">
				        {{ Session::get('flash_message') }}
				    </div>
				@endif
				@if($errors->any())
				    <div class="alert alert-danger">
				        @foreach($errors->all() as $error)
				            <p>{{ $error }}</p>
				        @endforeach
				    </div>
				@endif
				<div class="panel__button">
					<a href="{{ route('show.single.prompt', ['id_kelas' => $prompt->id_kelas, 'id_prompt' => $prompt->id]) }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
				</div>
				<div class="border">
					<div class="panel-prompt">
						<div class="panel-prompt__button">
							{!! Form::open(array('route' => 'destroy-prompt')) !!}
							{!! Form::hidden('id_prompt', $prompt->id) !!}
					        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] ) !!}
					        {!! Form::close() !!}
						</div>
						<div class="panel-prompt__question">
							{{$prompt->pertanyaan}}
						</div>
						<div class="panel-prompt__kata-kunci">
							@if(count($kataKunci) == 0)
							Tidak ada kata kunci untuk prompt ini.
							@else
							Kata kunci: @foreach($kataKunci as $index => $data)
														{{$data->katakunci}}@if($index!=count($kataKunci)-1),@endif
													@endforeach
							@endif
						</div>
						<div class="panel-prompt__details">
							Created on @datetime($prompt->created_at)
						</div>
					</div>
					<div class="panel-answers">
						@foreach($jawabanPrompt as $key => $value)
						<div class="panel-answer--single">
							
							<div class="panel-answer--single__avatar">	
									<img src="{{ url('/self-monitoring/public/img/avatar/') }}/{{ $jawabanUser[$key]->url_foto }}" alt="">
							</div>					
							
							<div class="panel-answer--single__user">
								<h3><strong>{{ $jawabanUser[$key]->name }} ({{ $jawabanUser[$key]->no_identitas == null ? $jawabanUser[$key]->role : $jawabanUser[$key]->no_identitas }})</strong></h3>
							</div>
							
							<div class="panel-answer--single__answer">
								{{$value->jawaban}}
							</div>
							
							<div class="panel-answer--single__details">
								Sent on @datetime($value->created_at)
							</div>
					
						</div>
						@endforeach
					</div>

					<div class="panel-answers">
						<div class="panel-answer--single">
							
							<div class="panel-answer--single__avatar">	
									<img src="{{ url('/self-monitoring/public/img/avatar/') }}/{{ Auth::user()->url_foto }}" alt="">
							</div>					
							
							<div class="panel-answer--single__user">
								<h3><strong>{{ Auth::user()->name }} ({{ Auth::user()->role }})</strong></h3>
							</div>
							
							<div class="panel-answer--single__answer">
								{!! Form::open(array('route' => 'store.answer.instruktur')) !!}
									{!! Form::hidden('id_prompt', $prompt->id) !!}
									{!! Form::hidden('id_user_sentTo', $id_user) !!}
									<div id="listFeedback" class="form-group">
									    {!! Form::label('feedback', 'Pilih feedback:', ['class' => 'control-label']) !!}
									    {!! Form::select('feedback', $feedbackPrompt, null, ['placeholder' => 'Pilih feedback', 'class' => 'form-control select-select2 select2-feedback', 'id' => 'answer1']); !!}
									</div>

									

									<div id="textJawab" class="form-group" style="display:none;">
									    {!! Form::label('jawaban', 'Feedback anda:', ['class' => 'control-label']) !!}
									    {!! Form::textarea('jawaban', null, ['placeholder' => 'Ketik feedback disini', 'class' => 'form-control', 'rows' => 5, 'id' => 'answer2']); !!}
									</div>

									<button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane-o"></i> Kirim Feedback</button>
									<button id="other1" class="btn btn-primary" type="button"><i class="fa fa-paper-plane-o"></i> Balas dengan feedback lain</button>
									<button id="other2" class="btn btn-primary" type="button" style="display:none;"><i class="fa fa-paper-plane-o"></i> Balas dengan feedback default</button>

								{!! Form::close() !!}
								
							</div>
						</div>
					</div>	
				</div>
				<br/>
				<br/>
			
			</div>
		</div>
	</div>

@endsection

@section('script')
  {{-- import style2 script --}}
  <script src="{{asset('js/select2/select2.min.js')}}"></script>

  <script type="text/javascript">
		$(document).ready(function() {
			$.fn.select2.defaults.set("theme", "bootstrap");
		  	$(".select-select2").select2();
		  	$(".select2-feedback").select2({
			  	allowClear: true,
			});
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#answer1').prop('required',true);
			$('#other1').click(function(){
				var id = this.value;
		    	$('#textJawab').show();
		    	$('#answer2').prop('required',true);
		    	$('#answer1').prop('required',false);
		    	$('#other1').hide();
		    	$('#other2').show();
		    	$('#listFeedback').hide();
		    	$('#answer1').val(null);
			});

			$('#other2').click(function(){
		    	$('#textJawab').hide();
		    	$('#other1').show();
		    	$('#other2').hide();
		    	$('#listFeedback').show();
		    	$('#answer2').prop('required',false);
		    	$('#answer1').prop('required',true);
			});
		});
	</script>
@endsection
