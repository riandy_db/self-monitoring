<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Self-Monitoring @yield('title')</title>

    {{-- insert favicon to the site--}}
    <link rel="icon" href="{{asset('img/sm-logo-small.png')}}" sizes="16x16 32x32" type="image/png">

    {{-- import bootstrap style --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    {{-- import default sass style --}}
    <link rel="stylesheet" href="{{asset('stylesheets/screen.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/print.css')}}">

    {{-- import custom styles --}}
    <link rel="stylesheet" href="{{asset('stylesheets/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/styles.css')}}">

    {{-- import select2 style --}}
    <link rel="stylesheet" href="{{asset('stylesheets/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/select2-bootstrap-theme/select2-bootstrap.min.css')}}">    

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{asset('css/creative.css')}}">
    



    

</head>
<body id="app-layout">
    <nav class="navbar-top">
        <div class="container-fluid header">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a href="{{ url('/') }}">
                    <img id="logo" src="{{asset('img/sm-logo-full.png')}}" alt="Self-Monitoring" height=50>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
 <!--                <ul class="nav navbar-nav navbar-main">
                    <li><a href="#">Tentang</a></li>
                    <li><a href="#">Publikasi</a></li>
                    <li><a href="#">Mitra</a></li>
                    <li><a href="#">Kontak</a></li>
                </ul> -->

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">


                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/description') }}">Deskripsi</a></li>
                        <li role="separator" class="divider-vertical"></li>
                        <li><a href="{{ url('/guide') }}">Panduan</a></li>
                        <li role="separator" class="divider-vertical"></li>
                        <li><a href="{{ url('/publication') }}">Publikasi</a></li>
                        <li role="separator" class="divider-vertical"></li>
                        <li><a href="{{ url('/login#contact') }}">Kontak</a></li>
                        <li role="separator" class="divider-vertical"></li>
                        <li><a href="{{ url('/register/user') }}">Registrasi</a></li>
                        <li role="separator" class="divider-vertical"></li>
                        <li><a href="{{ url('/login') }}">Login</a></li>                        
                    @else
                        @can('instruktur-access')
                            <li><a href="{{ url('/instruktur/kelas') }}">Kelas</a></li>
                            <li role="separator" class="divider-vertical"></li>
                            <li><a href="{{ url('/instruktur/prompt') }}">Prompt</a></li>
                            <li role="separator" class="divider-vertical"></li>
                        @endcan

                        @can('mahasiswa-access')
                            <li><a href="{{ url('/mahasiswa') }}">Prompt</a></li>
                            <li role="separator" class="divider-vertical"></li>
                        @endcan
                       

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle dropdown-user" data-toggle="dropdown" role="button" aria-expanded="false">
                                <img src="{{ asset('/self-monitoring/public/img/avatar/') }}/{{ Auth::user()->url_foto }}" class="img-square navbar-avatar">
                               <small>{{ Auth::user()->name }} ({{ Auth::user()->role != 'mahasiswa' ? Auth::user()->role : Auth::user()->no_identitas }}) <span class="caret"></span></small>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/change-avatar') }}"><i class="fa fa-btn fa-camera-retro"></i> Change Avatar</a></li>
								<li><a href="{{ url('/change-password') }}"><i class="fa fa-btn fa-key"></i> Change Password</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i> Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    
    @yield('sidebar')
    @yield('content')

    <footer class="{{(Auth::check() && Auth::user()->role != 'admin') ? 'footer-user ' : ' '}}navbar-fixed-bottom">
        <div class="container-fluid">
            <div class="text-right"> <sup>&copy</sup> 2016 Self-Monitoring Online | All Right Reserved</div>
        </div>
    </footer>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    
    {{-- import app(bootstrap) script --}}    
    <script src="{{asset('js/script.js')}}"></script>

    <!-- JavaScripts imported when a page implement it -->
    <script>
      $(document).ready(function() {
        
        $(window).scroll(function() {
          if ($('body').height() <= (($(window).height()) + $(window).scrollTop())) {
            $('.footer-user').addClass('show');
          } else {
            $('.footer-user').removeClass('show');
          }
        });
        if(($(window).scrollTop() == 0) && ($(window).height()>$('body').height()) ){
          $('.footer-user').addClass('show');
        }
      });    
    </script>
    @yield('script')

</body>
</html>
