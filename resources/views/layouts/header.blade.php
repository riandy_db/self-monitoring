@section('header')
<nav class="navbar navbar-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
    <a class="navbar-brand" href="#">
		<img src="{{asset('img/sm-logo-full.png')}}" alt="Self-Monitoring" height=50>
	</a>
    </div>

    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} ({{ Auth::user()->role }}) <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Profil</a></li>
            <li><a href="#"><i class="fa fa-wrench" aria-hidden="true"></i> Pengaturan</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i> Keluar</a></li>
          </ul>
        </li>
    </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
@stop
