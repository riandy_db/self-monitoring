<!-- Stored in resources/views/layouts/master.blade.php -->

<html>
    <head>
        <title>Self-Monitoring | @yield('title')</title>

{{-- insert favicon to the site--}}
		<link rel="icon" href="{{asset('img/sm-logo-small.png')}}" sizes="16x16 32x32" type="image/png">

{{-- import bootstrap style --}}
		<link rel="stylesheet" href="{{asset('css/app.css')}}">

{{-- import default sass style --}}
		<link rel="stylesheet" href="{{asset('stylesheets/screen.css')}}">
		<link rel="stylesheet" href="{{asset('stylesheets/print.css')}}">

{{-- import custom styles --}}
		<link rel="stylesheet" href="{{asset('stylesheets/font-awesome/font-awesome.css')}}">
		<link rel="stylesheet" href="{{asset('stylesheets/styles.css')}}">
		
<!--[if IE]
		<link rel="stylesheet" href="{{asset('stylesheets/ie.css')}}">
[endif]-->
    </head>
    <body>
		<div class="header">
			@yield('header')
		</div>
		<div class="container-fluid" style="height:100%">
			<div class="row">
				<div class="col-md-2 left-sidebar" style="height:100%">@yield('sidebar')</div>
				<div class="col-md-10" style="height:100%">@yield('content')</div>
			</div>
		</div>
		<div class="footer">
			<div class="container-fluid">
				<div class="text-right"> <sup>&copy</sup> 2016 Self-Monitoring Online | All Right Reserved</div>
			</div>
		</div>

{{-- import app(bootstrap) script --}}
		<script src="{{asset('js/app.js')}}"></script>
		<script src="{{asset('js/script.js')}}"></script>
    </body>
</html>