@extends('layouts.app')

@section('content')
	@section('title', ' | Prompt ')

	<div class="container">
		<div class="row">
			<div class="large-panel col-md-10 col-md-offset-1">
				<div class="kelas-prompt">{{$kelasInfo->nama_kelas}} <small>({{$kelasInfo->tahun_ajar_mulai}} {{$kelasInfo->batch}})</small> </div>
				<div class="prompt-topik">Topik: {{$prompt->topik}}</div>
			@if(Session::has('flash_message'))
		    <div class="alert alert-success">
		        {{ Session::get('flash_message') }}
		    </div>
			@endif
			@if($errors->any())
		    <div class="alert alert-danger">
		        @foreach($errors->all() as $error)
		            <p>{{ $error }}</p>
		        @endforeach
		    </div>
			@endif
				<div class="panel__button">
					<a href="{{ route('show-prompts-mahasiswa', $kelasInfo->id) }}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
				</div>
				<div class="border">	
				<div class="panel-prompt">
					@if($jawaban == null)
					<div class="panel-prompt__button">
						<button id="btn-reply" class="btn btn-sm btn-success btn-green"><i class="fa fa-reply"></i></button>
					</div>
					@endif
					<div class="panel-prompt__question">
						{{$prompt->pertanyaan}}
					</div>
					<div class="panel-prompt__details">
						Created on @datetime($prompt->created_at)
					</div>

					<div class="panel-answering hidden">	
					{!! Form::open(array('route' => 'store-answer-mahasiswa')) !!}
						{!! Form::hidden('id_prompt', $prompt->id) !!}
						<div class="form-group">
						    {!! Form::label('jawaban', 'Jawab:', ['class' => 'control-label']) !!}
						    {!! Form::textarea('jawaban', null, ['class' => 'form-control', 'rows' => 3]); !!}
						</div>

						<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Simpan Jawaban</button>
						<button id="btn-cancel" class="btn btn-danger" type="button"><i class="fa fa-times"></i> Batalkan</button>
						<!-- <a id="btn-cancel" class="btn btn-danger" href="#"><i class="fa fa-times"></i> Batalkan</a> -->

					{!! Form::close() !!}
					</div>
				</div>

				@if($jawaban != null)
				<div class="panel-answers">
				
					<div class="panel-answer--single">
						<!-- <div class="panel-answer--single__feedback">
							<button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>
						</div>	 -->
						<div class="panel-answer--single__avatar">	
								<img src="/self-monitoring/public/img/avatar/{{Auth::user()->url_foto}}" alt="">
						</div>				
						<div class="panel-answer--single__answer">
							{{$jawaban->jawaban}}
						</div>
						<div class="panel-answer--single__user">
							<small>by {{Auth::user()->name}} ({{Auth::user()->no_identitas}})</small>
						</div>
						<div class="panel-answer--single__details">
							Replied on @datetime($jawaban->created_at)
						</div>
					</div>					
				</div>
				@if($feedbackInstruktur != null)
				<div class="panel-answer--single feedback">			
					Feedback: <br>	
					{{$feedbackInstruktur->feedback_default}}

					@if($feedbackJawaban == null)
					<div class="panel-answering__feedback">	
					{!! Form::open(array('route' => 'store-balasan-mahasiswa')) !!}
						{!! Form::hidden('id_jawaban', $jawaban->id) !!}
						{!! Form::hidden('id_feedback', $feedbackInstruktur->id) !!}
						<div class="form-group">
					    {!! Form::textarea('balasan', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Tulis balasan feedback']); !!}
						</div>

						<button class="btn btn-primary" type="submit"><i class="fa fa-reply"></i> Balas</button>
						<!-- <a id="btn-cancel" class="btn btn-danger" href="#"><i class="fa fa-times"></i> Batalkan</a> -->

					{!! Form::close() !!}
					</div>
					@endif
				</div>
				@endif

				@if($feedbackJawaban)
					<div class="panel-feedback__replied">	
					Balasan: <br>	
						{{$feedbackJawaban}}
					</div>
				@endif

				@endif
			</div>
			</div>
		</div>
	</div>

@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#btn-reply').click(function(){
			$('#btn-reply').addClass('hidden');
	    $('.panel-answers').addClass('hidden');
	    $('.panel-answering').removeClass('hidden');
		});
		$('#btn-cancel').click(function(){
			$('#btn-reply').removeClass('hidden');
	    $('.panel-answers').removeClass('hidden');
	    $('.panel-answering').addClass('hidden');
		});
	});
</script>
@endsection
