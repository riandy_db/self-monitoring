@extends('layouts.app')

@section('content')
	@section('title', ' | Prompt ')

	<div class="container">
		<div class="row">
			<div class="large-panel col-md-10 col-md-offset-1">
				<div class="kelas-prompt">{{$kelasInfo->nama_kelas}} <small>({{$kelasInfo->tahun_ajar_mulai}} {{$kelasInfo->batch}})</small> </div>
				<div class="prompt-topik">Topik: {{$prompt->topik}}</div>
				@if(Session::has('flash_message'))
				    <div class="alert alert-success">
				        {{ Session::get('flash_message') }}
				    </div>
				@endif
				@if($errors->any())
				    <div class="alert alert-danger">
				        @foreach($errors->all() as $error)
				            <p>{{ $error }}</p>
				        @endforeach
				    </div>
				@endif
				<div class="panel__button">
					<a href="{{ route('show-prompts-mahasiswa', $kelasInfo->id) }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Kembali</a>
				</div>

				<div class="border">	
					<div class="panel-prompt">
						<div class="panel-prompt__question">
							{{$prompt->pertanyaan}}
						</div>
						<div class="panel-prompt__kata-kunci">
							@if(count($kataKunci) == 0)
							Tidak ada kata kunci untuk prompt ini.
							@else
							Kata kunci: @foreach($kataKunci as $index => $data)
														{{$data->katakunci}}@if($index!=count($kataKunci)-1),@endif
													@endforeach
							@endif
						</div>
						<div class="panel-prompt__details">
							Created on @datetime($prompt->created_at)
						</div>
						
					</div>

					<div class="panel-answers">				
						@foreach($jawabanPrompt as $key => $value)
						<div class="panel-answer--single">
							
							<div class="panel-answer--single__avatar">	
								<img src="{{ url('/self-monitoring/public/img/avatar/') }}/{{ $jawabanUser[$key]->url_foto }}" alt="">
							</div>					
							
							<div class="panel-answer--single__user">
								<h3><strong>{{ $jawabanUser[$key]->name }} ({{ $jawabanUser[$key]->no_identitas == null ? $jawabanUser[$key]->role : $jawabanUser[$key]->no_identitas }})</strong></h3>
							</div>
							
							<div class="panel-answer--single__answer">
								{{$value->jawaban}}
							</div>
							
							<div class="panel-answer--single__details">
								Sent on @datetime($value->created_at)
							</div>
					
						</div>
						@endforeach
		
					</div> 

					<div class="panel-answers">				
						<div class="panel-answer--single">
							<div class="panel-answer--single__avatar">	
								<img src="{{ url('/self-monitoring/public/img/avatar/') }}/{{Auth::user()->url_foto}}" alt="">
							</div>					
							
							<div class="panel-answer--single__user">
								<h3><strong>{{Auth::user()->name}} ({{Auth::user()->no_identitas}})</strong></h3>
							</div>
							
							<div class="panel-answer--single__answer">
								{!! Form::open(array('route' => 'store.answer.mahasiswa')) !!}
									{!! Form::hidden('id_prompt', $prompt->id) !!}
									<div class="form-group">
									    {!! Form::label('jawaban', 'Jawab:', ['class' => 'control-label']) !!}
									    {!! Form::textarea('jawaban', null, ['class' => 'form-control', 'rows' => 7, 'id' => 'answer']); !!}
									</div>

									<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Simpan Jawaban</button>
								{!! Form::close() !!}
							
							</div>

							
						</div>
					</div> 
				</div>
				<br/>
				<br/>
			</div>
		</div>
	</div>

@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#answer').prop('required',true);

	});
</script>
@endsection
