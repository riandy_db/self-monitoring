@extends('layouts.app')
@section('content')
@section('title', ' | Publikasi')
<div class="container">
	<div class="row">
		
		<h1>Self-Monitoring Online</h1>
		<div class="panel panel-default">
			<div class="panel-body">

				<p align="left">&emsp;&emsp;
					Pada jenjang pendidikan tinggi, mahasiswa sangat dianjurkan untuk menjadi pembelajar yang 
					mandiri dan reflektif. Mahasiswa harus bisa mengevaluasi cara ia belajar dibandingkan 
					dengan teman-temannya sebagai bagian dari proses <i style="font-style: italic;">self-regulated learning</i>. 
					<i style="font-style: italic;">Self-regulated learning</i> merupakan sebuah intervensi yang terdiri dari <i style="font-style: italic;">planning, monitoring</i>, 
					dan <i style="font-style: italic;">evaluating</i> proses belajar. Untuk meningkatkan proses-proses ini, <i style="font-style: italic;">self-monitoring 
					intervention</i> dapat digunakan sebagai strategi menganalis perkembangan mahasiswa dalam 
					proses belajarnya. 
				</p>

				<p align="left">&emsp;&emsp;
					Ada dua tujuan <i style="font-style: italic;">self-monitoring intervention</i>, yaitu <i style="font-style: italic;">self-observation</i> dan <i style="font-style: italic;">self-recording</i>. 
					<i style="font-style: italic;">Self-observation</i> merupakan sebuah aktivitas yang dilakukan oleh mahasiswa untuk mengamati 
					bagaimana proses belajar dirinya. <i style="font-style: italic;">Self-recording</i> merupakan sebuah aktivitas untuk merekam 
					kemampuan belajar mahasiswa. Untuk memfasilitasi intervensi dalam menggunakan teknik ini, 
					mahasiswa akan diberikan serangkaian <i style="font-style: italic;">sequential prompts</i> (pertanyaan) yang harus dijawab 
					untuk meninjau perkembangan pengetahuan mereka. 
				</p>

				<p align="left">&emsp;&emsp;
					Laboratorium Digital Library & Distance Learning, Fakultas Ilmu Komputer Universitas Indonesia 
					telah melakukan penelitian terkait <i style="font-style: italic;">self-monitoring intervention</i>. <i style="font-style: italic;">Self-Monitoring online tool</i> ini 
					merupakan hasil dari penelitian tersebut yang telah dikembangkan dengan melewati tahap 
					<i style="font-style: italic;">user experience</i> dan <i style="font-style: italic;">usability evaluation</i>. <i style="font-style: italic;">Self-Monitoring online tool</i> telah diuji menggunakan 
					User Experience Questionnaire (UEQ) dan System Usability Scale (SUS).
				</p>

				<p align="left">&emsp;&emsp;
					Pada <i style="font-style: italic;">tool</i> ini, Dosen sebagai fasilitator akan memberikan <i style="font-style: italic;">sequential prompts</i> 
					(pertanyaan berurutan) selama proses belajar berlangsung pada masa perkuliahan. 
					Mahasiswa diminta untuk menjawab pertanyaan-pertanyaan tersebut sesuai dengan yang mereka ketahui. 
					Pertanyaan dan jawaban akan ditampilkan berurutan sehingga mahasiswa dapat menganalisis 
					perkembangan pengetahuan mereka.  
				</p>

				

			</div>	
		</div>
	</div>
</div>
<br/>
<br/>
<br/>
@endsection