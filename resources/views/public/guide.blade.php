@extends('layouts.app')
@section('content')
@section('title', ' | Panduan')
<div class="container">
  <h1>Panduan Penggunaan Self-Monitoring Online</h1>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="padding-bottom: 30px">
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" href="#" aria-expanded="true" aria-controls="collapseOne">
          1. Role Siswa/Mahasiswa dan Instruktur
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <div class="guide"> 
          <h2 class="guide-header">Login dan Logout</h2>
          <p class="guide-body">
          Untuk melakukan login ke dalam sistem, buka url http://monitoring.cs.ui.ac.id. Maka akan muncul form login seperti berikut:
          </p>
          <div class="guide-img">
            <img src="/img/guide/login.PNG" alt="">
          </div>
          <p class="guide-body">
            Isi field Username dengan username Anda dan isi field Password dengan password yang bersesuaian, kemudian tekan tombol ‘Enter’ atau pilih login. Jika akun Anda telah tervalidasi, maka Anda akan diarahkan ke halaman utama.
          </p>
          <p class="guide-body">
            Jika Anda ingin logout dari sistem, pilih menu dropdown yang ada di pojok kanan atas (klik nama Anda) lalu pilih menu logout seperti pada gambar berikut:
          </p>
          <div class="guide-img">
            <img src="/img/guide/menu.PNG" alt="">
          </div>
          <p class="guide-body">
            Ketika Anda berhasil melakukan logout, Anda akan di arahkan ke halaman login.
          </p>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Mengubah Password</h2>
          <p class="guide-body">
          Untuk mengubah password Anda harus sudah melakukan login terlebih dahulu. Selanjutnya, pilih menu dropdown yang ada di pojok kanan atas (klik nama Anda) lalu pilih menu Change Password seperti pada gambar berikut:
          </p>
          <div class="guide-img"><img src="/img/guide/menu.PNG" alt=""></div>
          <p class="guide-body">
            Anda akan diarahkan ke halaman yang berisi form untuk mengubah password Anda. Isi field yang bersesuaian:
          </p>
          <div class="guide-img"><img src="/img/guide/password.PNG" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Kemudian pilih tombol Ubah Password untuk menyimpan perubahan password.
          </p>
        </div>

        <div class="guide"> 
          <h2 class="guide-header">Mengubah Foto Profil</h2>
          <p class="guide-body">
            Untuk mengubah foto profil, Anda harus sudah melakukan login terlebih dahulu. Selanjutnya, pilih menu dropdown yang ada di pojok kanan atas (klik nama Anda) lalu pilih menu Change Avatar seperti pada gambar berikut:
          </p>
          <div class="guide-img"><img src="/img/guide/menu.PNG" alt=""></div>
          <p class="guide-body">
            Anda akan diarahkan ke halaman yang berisi form untuk mengubah foto profil Anda. Pilih tombol Choose File untuk memilih foto profil anda yang baru.
          </p>
          <div class="guide-img"><img src="/img/guide/avatar.PNG" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Kemudian pilih tombol Simpan untuk menyimpan perubahan foto profil.
          </p>
        </div>

      </div>
    </div>
  </div>
  

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo" href="#" aria-expanded="false" aria-controls="collapseTwo">
          2. Role Instruktur
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <div class="panel-body">
        <div class="guide"> 
          <h2 class="guide-header">Melihat Daftar Kelas yang Dikelola</h2>
          <p class="guide-body">
          Untuk melihat daftar kelas yang Anda kelola, Anda harus login sebagai instruktur. Kemudian pilih menu Kelas pada navbar di atas seperti pada gambar:
          </p>
          <div class="guide-img">
            <img src="/img/guide/kelas.png" alt="" height="200" width="500">
          </div>
          <p class="guide-body">
            Maka daftar kelas yang Anda kelola akan muncul.
          </p>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Menambahkan Daftar Kelas yang Dikelola</h2>
          <p class="guide-body">
          Untuk menambahkan daftar kelas yang ingin Anda kelola, pilih menu Tambah Kelas pada halaman daftar kelas seperti pada gambar:
          </p>
          <div class="guide-img"><img src="/img/guide/add-kelas.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Halaman dengan form untuk menambahkan kelas akan muncul, isi field yang bersesuaian. Anda dapat mengosongkan daftar mahasiswa terlebih dahulu.
          </p>
          <div class="guide-img"><img src="/img/guide/add-kelas-2.PNG" alt="" height="200" width="500"></div>
          <p class="guide-body">
          Pilih tombol Tambah kelas untuk menambahkan kelas. Jika berhasil notifikasi sukses akan muncul, pilih tombol Kembali untuk kembali ke halaman utama.
          </p>
          <div class="guide-img"><img src="/img/guide/add-kelas-3.PNG" alt="" height="100" width="500"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Menghapus Kelas yang Dikelola</h2>
          <p class="guide-body">
          Untuk menghapus kelas yang dikelola, Anda harus berada pada halaman daftar kelas yang dikelola. Hover kursor Anda pada kelas yang ingin dihapus seperti pada gambar, lalu pilih tombol dengan icon trashbin:
          </p>
          <div class="guide-img"><img src="/img/guide/delete-kelas.png" alt="" height="250" width="500"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Melihat Daftar Mahasiswa di Suatu Kelas</h2>
          <p class="guide-body">
         Untuk melihat daftar mahasiswa pada suatu kelas, Anda harus sudah mengelola suatu kelas. Pada halaman daftar kelas yang Anda kelola, pilih salah satu kelas yang ingin Anda lihat:
          </p>
          <div class="guide-img"><img src="/img/guide/view-kelas.png" alt="" height="250" width="500"></div>
          <p class="guide-body">
            Maka daftar mahasiswa yang telah ditambahkan pada kelas tersebut akan muncul seperti berikut:
          </p>
          <div class="guide-img"><img src="/img/guide/list-siswa.PNG" alt="" height="200" width="800"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Menambah Daftar Mahasiswa pada Suatu Kelas</h2>
          <p class="guide-body">
         Untuk menambahkan mahasiswa pada suatu kelas, Anda harus melihat daftar mahasiswa yang ada pada kelas tersebut dengan cara memilih kelas yang ingin Anda tambahkan mahasiswanya pada halaman daftar kelas.
          </p>
          <div class="guide-img"><img src="/img/guide/view-kelas.png" alt="" height="250" width="500"></div>
          <p class="guide-body">
            Kemudian pada halaman detail suatu kelas, pilih menu tambah yang berada di atas kanan tabel daftar mahasiswa.
          </p>
          <div class="guide-img"><img src="/img/guide/add-siswa.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Halaman dengan form untuk menambahkan mahasiswa akan muncul. Isi field yang bersesuaian, Anda bisa memasukkan mahasiswa berdasarkan nama mereka atau NPM mereka.
          </p>
          <div class="guide-img"><img src="/img/guide/add-siswa-2.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Selanjutnya pilih tombol Tambah Mahasiswa untuk menyimpan perubahan. Notifikasi sukses akan muncul setelah mahasiswa berhasil ditambahkan.
          </p>
          <div class="guide-img"><img src="/img/guide/add-siswa-3.png" alt="" height="150" width="500"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Menghapus Mahasiswa dari Suatu Kelas</h2>
          <p class="guide-body">
         Untuk menghapus mahasiswa dari suatu kelas, Anda harus berada pada halaman yang menampilkan daftar mahasiswa. Dari halaman daftar kelas, pilih salah satu kelas yang ingin Anda lihat daftar mahasiswanya.
          </p>
          <div class="guide-img"><img src="/img/guide/view-kelas.PNG" alt="" height="250" width="500"></div>
          <p class="guide-body">
            Pilih tombol hapus pada baris mahasiswa yang ingin dihapus dari kelas, seperti pada gambar berikut:
          </p>
          <div class="guide-img"><img src="/img/guide/delete-siswa.PNG" alt="" height="250" width="500"></div>
          <p class="guide-body">
            Jika sukses, notifikasi mahasiswa berhasil dihapus akan muncul.
          </p>
          <div class="guide-img"><img src="/img/guide/delete-siswa-2.PNG" alt="" height="150" width="500"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Melihat Daftar Prompt di Suatu Kelas</h2>
          <p class="guide-body">
         Untuk melihat daftar prompt di suatu kelas, pilih menu prompt pada navbar di atas.
          </p>
          <div class="guide-img"><img src="/img/guide/prompt.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
          Maka halaman dengan daftar prompt akan muncul, pilih salah satu kelas pada panel kiri seperti pada gambar:
          </p>
          <div class="guide-img"><img src="/img/guide/list-prompt.png" alt="" height="200" width="500"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Menambah Prompt pada Suatu Kelas</h2>
          <p class="guide-body">
         Untuk menambahkan prompt pada suatu kelas, Anda harus berada pada halaman daftar prompt dengan cara memilih menu prompt pada navbar di bagian atas.
          </p>
          <div class="guide-img"><img src="/img/guide/prompt.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
           Kemudian pilih menu tambah prompt yang berada pada atas kanan tabel daftar prompt.
          </p>
          <div class="guide-img"><img src="/img/guide/add-prompt.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Halaman dengan form untuk membuat prompt baru akan muncul, isi field yang bersesuaian.
          </p>
          <div class="guide-img"><img src="/img/guide/add-prompt-2.png" alt="" height="500" width="500"></div>
          <p class="guide-body">
            Jangan lupa untuk menambahkan feedback default untuk memberikan feedback pada jawaban mahasiswa nantinya. Jika feedback lebih dari satu, pisahkan dengan semicolon (;). Pilih tombol tambah prompt untuk menyimpan perubahan.
          </p>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Melihat Daftar Jawaban pada Suatu Prompt</h2>
          <p class="guide-body">
            Pada halaman daftar Prompt, pilih salah satu prompt yang tersedia seperti pada gambar:
          </p>
          <div class="guide-img"><img src="/img/guide/list-prompt-2.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Halaman daftar siswa/mahasiswa yang telah menjawab prompt akan muncul seperti berikut:
          </p>
          <div class="guide-img"><img src="/img/guide/prompt-answer.PNG" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Jika salah satu siswa/mahasiswa dipilih, akan muncul jawaban siswa/mahasiswa tersebut.
          </p>
          <div class="guide-img"><img src="/img/guide/prompt-answer-2.PNG" alt="" height="200" width="500"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Memberikan Feedback Jawaban Prompt</h2>
          <p class="guide-body">
            Untuk memberikan feedback suatu jawaban prompt, Anda sudah harus memilih salah satu prompt pada suatu kelas dan memilih siswa/mahasiswa yang sudah menjawab prompt. Dari jawaban yang ada, terdapat field untuk memberikan feedback dibagian bawah jawaban (lihat gambar berikut).
          <br/>
            Feedback sudah tersedia sesuai yang telah Anda buat saat membuat prompt, pilih kirim feedback untuk memberikan feedback.
          </p>
          <div class="guide-img"><img src="/img/guide/feedback.PNG" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Anda juga dapat membuat feedback baru dengan memilih 'Balas dengan feedback lain'.
          </p>
          <div class="guide-img"><img src="/img/guide/feedback-1.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Masukkan feedback Anda pada field yang tersedia, lalu pilih kirim feedback.
          </p>
          <div class="guide-img"><img src="/img/guide/feedback-2.PNG" alt="" height="200" width="500"></div>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Menghapus Prompt</h2>
          <p class="guide-body">
         Untuk menghapus prompt, Anda sudah harus memilih salah satu prompt pada suatu kelas. Pilih tombol icon trashbin untuk menghapus prompt.
          </p>
          <div class="guide-img"><img src="/img/guide/delete-prompt.PNG" alt="" height="200" width="700"></div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree" href="#" aria-expanded="false" aria-controls="collapseThree">
          3. Role Siswa/Mahasiswa
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <div class="guide"> 
          <h2 class="guide-header">Melihat Daftar Prompt pada Suatu Kelas</h2>
          <p class="guide-body">
            Untuk melihat daftar prompt pada suatu kelas, Anda harus login sebagai mahasiswa. Pada halaman utama pilih kelas pada panel sebelah kiri seperti pada gambar:
          </p>
          <div class="guide-img"><img src="/img/guide/prompt-siswa.png" alt="" height="200" width="500"></div>
          <p class="guide-body">
            Daftar pertanyaan prompt pada kelas tersebut akan muncul.
          </p>
        </div>
        <div class="guide"> 
          <h2 class="guide-header">Menjawab Pertanyaan Prompt</h2>
          <p class="guide-body">
            Untuk menjawab pertanyaan prompt, pilih salah satu kelas lalu pilih prompt yang ingin dijawab pada halaman utama.
            <br/>
            Field untuk memasukkan jawaban terdapat dibawah pertanyaan prompt. Pilih simpan jawaban untuk menjawab prompt tersebut.
          </p>
          <div class="guide-img"><img src="/img/guide/jawaban.PNG" alt="" height="200" width="500"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection