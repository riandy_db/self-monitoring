@extends('layouts.app')
@section('content')
@section('title', ' | Publikasi')
<div class="container">
	<div class="row">
		<h1>Publikasi terkait Self-Monitoring Online</h1>

		<div class="panel panel-default">
			<div class="panel-body">
				Harry B. Santoso, Isnaeni Nurrohmah, Suci Fadhilah, & Wade H. Goodridge
				(2017). <b> Evaluating and Redesigning the Self-Monitoring Tool</b>. International
				Journal on Advanced Science, Engineering and Information Technology
				(IJASEIT). 7(1), 228-234. <a target="_blank" href="http://ijaseit.insightsociety.org/index.php?option=com_content&view=article&id=9&Itemid=1&article_id=1526">(Click here to download or link to the paper)</a>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				Harry B. Santoso, Isnaeni Nurrohmah, Suci Fadhilah, & Wade Goodridge.
				(2016). <b>The Usability and User Experience Evaluation of Web-based Online
				Self-Monitoring Tool: Case Study Human-Computer Interaction Course</b>. Paper
				presented at the 4th International Conference on User Science and
				Engineering 2016 (i-USEr 2016), Melaka, MALAYSIA. <a target="_blank" href="http://ieeexplore.ieee.org/document/7857946/">(Click here to download or link to the paper)</a>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				Isnaeni Nurrohmah, Harry B. Santoso, Kasiyah Junus, & Lia Sadita. (2015).
				<b>Development and Usability Evaluation of Web-Based Self-Monitoring Tool</b>.
				Paper presented at The 23rd International Conference on Computers in
				Education (ICCE 2015), Hangzhou, CHINA.
				
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				Kasiyah M. Junus, Harry B. Santoso, & Lia Sadita. (2014). <b>The use of	
				Self-Monitoring Tools for Linear Algebra Course in Student Centered
				e-Learning Environment</b>. Paper presented at the 44th Frontiers in Education
				2014, Madrid, SPAIN.
				<a target="_blank" href="http://ieeexplore.ieee.org/document/7044296/?reload=true&arnumber=7044296">(Click here to download or link to the paper)</a>
			</div>
		</div>

	</div>


</div>
@endsection